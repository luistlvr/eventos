</br>

<div class="large-10 columns">
	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-6 columns">
		<?php 
			echo $this->Form->create('Place', array('class' => 'custom', 'type' => 'get'));
			echo $this->Form->hidden('change_flag', array('value' => true));
			echo $this->Form->input('title_search', array('type' => 'text', 'placeholder' => __('Buscar'), 'label' => false, 'id' => 'title_search'));
		?>
	</div>

	<div class="large-2 columns">
		<?php 
			echo $this->Form->submit(__('Buscar'), array('name' => 'ok', 'div' => false, 'class' => 'button small success', 'id' => 'search')); 
		?>
	</div>

	<div class="large-4 columns" align="right">
		<?php echo $this->Html->link(
						$this->Html->image("agregar2_ico.png") . " " . __("Agregar Lugar"), 
						array('action' => 'add', 'admin' => true), 
						array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)); ?>
	</div>

	<div class="large-12 columns">
		<div class="inline_labels">
		<?php 
			echo $this->Form->end();
		?>
		</div>
	</div>

	<div class="large-12 columns">
		<table width="100%">
		<thead>
			<tr>
				<th width="300"><?php echo $this->Paginator->sort('name', __('Nombre', true)); ?></th>
				<th width="250"><?php echo $this->Paginator->sort('speaker', __('Capacidad', true)); ?></th>
		    </tr>
		</thead>
		<tbody>
		    <?php foreach($places as $place): ?>
			    <tr>
					<td><?php echo $this->Html->link($place['Place']['name'], 
					    				array('action' => 'view', $place['Place']['id'], 'admin' => true), 
					    				array('escape' => false,/*'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true',*/ 'title' => 'Ver'));  ?></td>
					<td><?php echo $place['Place']['capacity']; ?></td>
			    </tr>
			<?php endforeach; ?>
		</tbody>
		</table>

		<ul class="pagination">
	    	<?php
	        	echo $this->Paginator->prev(__('anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'unavailable','disabledTag' => 'a'));
	            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'current','tag' => 'li','first' => 1));
	            echo $this->Paginator->next(__('siguiente'), array('tag' => 'li','currentClass' => 'unavailable'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	        ?>
	    </ul>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// Caching the movieName textbox:
		var title = $('#title_search');

		// Using jQuery UI's autocomplete widget:
		title.autocomplete({
			minLength    : 1,
			source       : '<?php echo $this->Html->url('/'); ?>' + 'admin/places/index',
			select: function(event, ui) {
	        }
		});
	});
	
	$('#title_search').keyup(function (e) {
	    if(e.keyCode == 13) {
	        e.preventDefault();
			$('PlaceAdminIndexForm').submit();
	    }
	});
</script>