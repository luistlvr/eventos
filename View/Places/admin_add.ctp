</br>

<?php echo $this->Form->create('Place', array('class' => 'custom', 'type' => 'file')); ?>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>
	
	<div class="large-8 columns">
		<?php echo $this->Form->input('name', array('label' => '<b>'.__('Nombre*', true).'</b>', 'placeholder' => __('Nombre', true))); ?>	
	</div>

	<div class="large-4 columns">
		<?php echo $this->Form->input('capacity', array('label' => '<b>'.__('Capacidad*', true).'</b>', 'placeholder' => __('Capacidad', true))); ?>
	</div>

	<div class="large-12 columns">
 		<?php
			echo $this->Form->submit(__('Agregar', true), array('name' => 'ok', 'div' => false, 'class' => 'button', 'id' => 'load')) . '&nbsp;&nbsp;';
			echo $this->Html->link(__('Cancelar', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
			echo $this->Form->end(); 
		?>

		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php if($isAjax): 
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
		echo $this->Html->script('datepair.js');
?>
	<script>
		$(document).foundation();
	</script>

	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-places").addClass('active');
	});

	$(function () {
		window.prettyPrint && prettyPrint();
		$('#dp').fdatepicker({
			format: 'yyyy-mm-dd'
		});
	});

	$('#load').on('click', function() {
		var myForm = document.getElementById("PlaceAdminAddForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>