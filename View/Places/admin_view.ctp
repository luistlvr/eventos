<?php
	$dias   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$punish = array("No", "Si");
?>

</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-8 columns">
		<h3><b><?php echo $place['Place']['name']; ?></b></h3>
	</div>

	<div class="large-4 columns">
		<p>
			<b><?php echo __('Capacidad: ', true); ?></b>
			<?php echo $place['Place']['capacity']; ?>
		</p>
	</div>

	<div class="large-6 columns">
		<p>
			<b><?php echo __('Creado: ', true); ?></b>
			<?php echo $place['Place']['created']; ?>
		</p>
	</div>
	
	<div class="large-12 columns"> 
	<?php
    	echo $this->Html->link(
    					$this->Html->image("edit.png") . " " . __("Editar"), 
    					array('action' => 'edit', $place['Place']['id'], 'admin' => true), 
    					array('escape' => false/*, 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'*/)). '&nbsp;&nbsp;'; 

    	echo $this->Form->postLink(
    					$this->Html->image("delete.png") . " " . __("Borrar"), 
    					array('action' => 'delete', $place['Place']['id'], 'admin' => true), 
    					array('escape' => false, 'confirm' => 'Está seguro?')) . '&nbsp;&nbsp;'; 
    ?>
	</div>
	
	<div class="large-12 columns" style="padding-top: 10px;"> 
		<?php
			echo $this->Html->link(__('Volver', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary')); 
		?>
	</div>

<!-- Small Modal -->
<div id="secondModal" class="reveal-modal small">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-places").addClass('active');
	});
</script>