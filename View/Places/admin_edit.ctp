</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<?php 
		echo $this->Form->create('Place', array('class' => 'custom')); 
		echo $this->Form->input('Place.id', array('type' => 'hidden'));
	?>

	<div class="large-8 columns">
	<?php
		echo $this->Form->input('name', array('label' => '<b>'.__('Nombre*', true).'</b>', 'placeholder' => __('Nombre', true)));
	?>
	</div>

	<div class="large-4 columns">
	<?php
		echo $this->Form->input('capacity', array('label' => '<b>'.__('Capacidad*', true).'</b>', 'placeholder' => __('Capacidad', true)));
	?>
	</div>

	<div class="large-12 columns">
	<?php
		echo $this->Form->submit(__('Actualizar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
		echo $this->Html->link(__('Cancelar', true), array('action' => 'view', $this->request->data['Place']['id'], 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
		echo $this->Form->end(); 
	?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php if($isAjax): 
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
?>
	<script>
		$(document).foundation();
	</script>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-places").addClass('active');
	});
	
	$('#load').on('click', function() {
		var myForm = document.getElementById("PlaceAdminEditForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>
