<?php if($isAjax): ?>
	<?php 
		echo $this->Html->script('jquery.uploadify.min.js');
		echo $this->Html->css('uploadify');
	?>
	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
        <h3><?php echo $student['Student']['fullname']; ?></h3>
    </div>

    <hr>

	<?php echo $this->Form->create('Message', array('type' => 'file')); ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('title', array('label' => '<b>'.__('Título', true).'</b>', 'placeholder' => __('Título', true))); ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('message', array('label' => '<b>'.__('Mensaje', true).'</b>', 'placeholder' => __('Mensaje', true), 'type' => 'textarea')); ?>
	</div>

	<div class="large-12 columns" style="padding-top: 10px;">
		<?php echo __("1 Archivo Max."); ?>
		<div id="queue"></div>
		<?php 
			echo $this->Form->input('file', array('type'=>'file', 'label' => false, 'name' => 'file_upload', 'id' => 'file_upload'));
			echo $this->Form->input('attach', array('type'=>'hidden', 'id' => 'file')); 
		?>
		<p><b id="attach"></b></p>
	</div>

	<div class="large-12 columns">
	<?php
		echo $this->Form->submit(__('Enviar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
		echo $this->Html->link(__('Cancelar', true), array('action' => 'view', $id, 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
		echo $this->Form->end(); 
	?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php echo $this->Html->script('tinymce.min.js'); ?>
<script type="text/javascript">  
	$(document).ready(function() {
		$("#manage-students").addClass('active');
	});

  $('#load').on('click', function() {
		var myForm = document.getElementById("MessageAdminSendMailForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});

  <?php $timestamp = time(); ?>
  var path = "<?= $this->Html->url('/app/webroot/uploadify/'); ?>";

  $(function() {
        $('#file_upload').uploadify({
        	'formData' : {
        		'timestamp' : '<?php echo $timestamp; ?>',
        		'token'     : '<?php echo md5('unique_salt' . $timestamp); ?>'
        	},
        	'fileSizeLimit' : '10MB',
            'swf'      : path + 'uploadify.swf',
            'uploader' : path + 'uploadify.php',
            'buttonText' : '<?php echo __('Adjuntar'); ?>',
            'onUploadComplete' : function(file, errorCode, errorMsg, errorString) {
	            document.getElementById("attach").innerHTML = file['name'];
	            $('#file').val(file['name']);
	        } 
        });
    }); 

  	tinymce.init({
    	selector: "textarea"
 	});
</script>

