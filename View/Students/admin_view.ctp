</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<h3><b><?php echo $student['Student']['fullname']; ?></b></h3>
	</div>

	<div class="large-5 columns">
		<p>
			<b><?php echo __('Código: ', true); ?></b>
			<?php echo $student['Student']['code']; ?>
		</p>
	</div>

	<div class="large-7 columns">
		<p>
			<b><?php echo __('Correo: ', true); ?></b>
			<?php echo $student['Student']['email']; ?>
		</p>
	</div>

	<div class="large-5 columns">
		<p>
			<b><?php echo __('Semestre: ', true); ?></b>
			<?php echo $student['Student']['semester']; ?>
		</p>
	</div>	

	<div class="large-7 columns">
		<p>
			<b><?php echo __('Creado: ', true); ?></b>
			<?php echo $student['Student']['created']; ?>
		</p>
	</div>

	<div class="large-4 columns">
		<p>
			<b><?php echo __('Estado: ', true); ?></b>
			<?php echo $states[$student['Student']['state']]; ?>
		</p>
	</div>

	<div class="large-8 columns">
		<p>
			<?php 
				if($student['Student']['state'] == 0) 
					echo '<b>' . __('Fecha de Castigo: ', true) . '</b>' .h(date('Y-m-d h:i A', strtotime($student['Student']['punishment_date']))); 
			?>
		</p>
	</div>

	<?php if(!empty($student['Event'])): ?>
	<hr>
	<div class="large-12 columns">
		<h6><?php echo __('Créditos'); ?></h6>
		<table>
		<thead>
			<tr>
				<th width="450"><?php echo __('Título'); ?></th>
				<th width="300"><?php echo __('Fecha'); ?></th>
				<th width="150"><?php echo __('Creditos'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php
			$credits = 0;
			foreach ($student['Event'] as $event): ?>
				<tr>
					<td><?php echo h($event['title']); ?></td>
					<td><?php echo h(date('Y-m-d h:i A', strtotime($event['datetime']))); ?></td>
					<td align="middle"><?php echo h($event['credits']); $credits += $event['credits']; ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td><b>Total</b></td>
				<td></td>
				<td align="middle"><?php  echo $credits; ?></td>
			</tr>
		</tbody>  
		</table>
	</div>
	<?php endif; ?>

<?php if(!$isAjax): ?>
		<div class="large-12 columns"> 
		<?php
	    	echo $this->Html->link(
	    					$this->Html->image("edit.png") . " " . __("Editar"), 
	    					array('action' => 'edit', $student['Student']['id'], 'admin' => true), 
	    					array('escape' => false/*, 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;'; 

	    	echo $this->Html->link($this->Html->image("message.png") . " " . __('Mensaje'),
							array('action' => 'send_mail', $student['Student']['id'], 'admin' => true),
						array('escape' => false/*, 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;';

	    	echo $this->Form->postLink(
	    					$this->Html->image("delete.png") . " " . __("Borrar"), 
	    					array('action' => 'delete', $student['Student']['id'], 'admin' => true), 
	    					array('escape' => false, 'confirm' => 'Está seguro?')) . '&nbsp;&nbsp;'; 

	    	echo $this->Html->link($this->Html->image("pdf.png") . " " . __("Pdf"), 
			        		array('action' => 'view_pdf', 'ext' => 'pdf', $student['Student']['id'], 'admin' => true),
			        		array('escape' => false, 'target' => '_blank')); 		
	    ?>
		</div>

		<div class="large-12 columns" style="padding-top: 10px;"> 
			<?php
				echo $this->Html->link(__('Volver', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary')); 
			?>
		</div>
	</div>
<?php endif; ?>

<!-- Small Modal -->
<div id="secondModal" class="reveal-modal small">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-students").addClass('active');
	});

	$('#load').on('click', function() {
		var myForm = document.getElementById("EventAdminTakeListForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>


