</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<?php 
		echo $this->Form->create('Student', array('class' => 'custom')); 
		echo $this->Form->input('Student.id', array('type' => 'hidden'));
		echo $this->Form->input('punishment_time', array('type' => 'hidden')); 
	?>

	<div class="large-6 columns">
	<?php
		echo $this->Form->input('names', array('label' => '<b>'.__('Nombres*', true).'</b>', 'placeholder' => __('Nombres', true)));
	?>
	</div>

	<div class="large-6 columns">
	<?php
		echo $this->Form->input('code', array('label' => '<b>'.__('Código*', true).'</b>', 'placeholder' => __('Código', true)));
	?>
	</div>

	<div class="large-6 columns">
	<?php
		echo $this->Form->input('first_surname', array('label' => '<b>'.__('Apellido Paterno*', true).'</b>', 'placeholder' => __('Apellido Paterno')));
	?>
	</div>

	<div class="large-6 columns">
	<?php
		echo $this->Form->input('second_surname', array('label' => '<b>'.__('Apellido Materno*', true).'</b>', 'placeholder' => __('Apellido Materno')));
	?>
	</div>

	<?php if($only_ucsp_email): ?>
		<div class="large-8 small-10 columns">
			<div class="row collapse">
				<label><b><?php echo __('Correo*', true); ?></b></label>
				<div class="small-6 columns">
				<?php echo $this->Form->input('mail', array('label' => false, 'placeholder' => __('Correo', true))); ?>
				</div>
				<div class="small-6 columns">
				  <span class="postfix radius"><?php echo __("@ucsp.edu.pe") ?></span>
				</div>
			</div>
		</div>
	<?php else: ?>
		<div class="large-8 small-10 columns">
			<?php echo $this->Form->input('email', array('label' => '<b>'.__('Correo*', true).'</b>', 'placeholder' => __('Correo', true))); ?>
		</div>
	<?php endif; ?>

	<div class="large-4 columns">
	<?php
		echo $this->Form->input('semester', array('label' => '<b>'.__('Semestre*', true).'</b>', 'type'=>'select', 'options' => $semester_options));
	?>
	</div>

	<div class="large-6 columns">
	<?php
		echo $this->Form->input('state', array('label' => '<b>'.__('Estado*').'</b>', 'placeholder' => __('Estado'), 'type'=>'select', 'options' => $state_options));
	?>
	</div>

	<div class="large-6 columns">
	<?php 
		echo $this->Form->input('punishment_date', array('label' => '<b>'.__('Fecha de Castigo').'</b>', 'placeholder' => __('Fecha de Castigo'), 'type' => 'text', 'id' => 'dp', 'class' => 'span2')); 
	?>
	</div>

	<div class="large-12 columns">
	<?php
		echo $this->Form->submit(__('Actualizar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
		echo $this->Html->link(__('Cancelar', true), array('action' => 'view', $this->request->data['Student']['id'], 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
		echo $this->Form->end(); 
	?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php if($isAjax): 
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
?>
	<script>
		$(document).foundation();
	</script>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-students").addClass('active');
	});

	$(function () {
		window.prettyPrint && prettyPrint();
		$('#dp').fdatepicker({
			format: 'yyyy-mm-dd'
		});
	});

	$('#load').on('click', function() {
		var myForm = document.getElementById("StudentAdminEditForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>
