</br>

<div class="large-10 columns">
	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-4 columns">
	<?php 
		echo $this->Form->create('Student', array('novalidate' => true, 'type' => 'get'));
		echo $this->Form->hidden('change_flag', array('value' => true));
		echo $this->Form->input('names', array('type' => 'text', 'placeholder' => __('Nombres'), 'label' => false, 'id' => 'names'));
	?>
	</div>

	<div class="large-3 columns">
	<?php 
		echo $this->Form->input('first_surname', array('type' => 'text', 'placeholder' => __('Apellido Paterno'), 'label' => false, 'id' => 'first_surname'));
	?>
	</div>

	<div class="large-3 columns">
	<?php 
		echo $this->Form->input('second_surname', array('type' => 'text', 'placeholder' => __('Apellido Materno'), 'label' => false, 'id' => 'second_surname'));
	?>
	</div>

	<div class="large-2 columns">
		<?php 
			echo $this->Form->submit(__('Buscar'), array('name' => 'ok', 'div' => false, 'class' => 'button small success', 'id' => 'search')); 
		?>
	</div>

	<div class="large-8 columns">
		<div class="inline_labels">
		<?php 
			echo $this->Form->radio('state', array('0' => 'Penalizados', '1' => 'Normales', '2' => 'Todos'), array('legend' => false, 'div' => false, 'default' => '2'/*, 'onclick' => 'this.form.submit();'*/)); 
			echo $this->Form->end();
		?>
		</div>
	</div>

	<div class="large-4 columns" align="right">
		<?php echo $this->Html->link(
						$this->Html->image("agregar2_ico.png") . " " . __("Agregar Estudiante"), 
						array('action' => 'add', 'admin' => true), 
						array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)); ?>
	</div>
	
	<?php if(!empty($students)) : ?>

	<div class="large-12 columns">
		<table width="100%">
		<thead>
			<tr>
				<th width="100"><?php echo $this->Paginator->sort('code', __('Código')); ?></th>
		  		<th width="350"><?php echo $this->Paginator->sort('fullname', __('Nombre')); ?></th>
				<th width="300"><?php echo $this->Paginator->sort('email', __('Correo')); ?></th>
				<th width="50"><?php echo $this->Paginator->sort('semester', __('Semestre')); ?></th>
				<!--<th width="300"><?php echo __("Acciones"); ?></th>-->
		    </tr>
		</thead>
		<tbody> 
			<?php foreach($students as $student): ?>
				<tr>
					<td><?php echo $student['Student']['code']; ?></td>
					<td><?php echo $this->Html->link($student['Student']['fullname'], 
					    					array('action' => 'view', $student['Student']['id'], 'admin' => true), 
					    					array('escape' => false, /*'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true',*/ 'title' => 'Ver')); ?></td>
					<td><?php echo $student['Student']['email']; ?></td>
					<td align="middle"><?php echo $student['Student']['semester']; ?></td>
					<!--<td>
						<?php 
							echo $this->Html->link(
					    					$this->Html->image("edit-find.png") . " " . __("Ver"), 
					    					array('action' => 'view', $student['Student']['id'], 'admin' => true), 
					    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')) . '&nbsp;&nbsp;'; 

				        	echo $this->Html->link(
					    					$this->Html->image("edit.png") . " " . __("Editar"), 
					    					array('action' => 'edit', $student['Student']['id'], 'admin' => true), 
					    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')). '&nbsp;&nbsp;'; 

				        	echo $this->Form->postLink(
					    					$this->Html->image("delete.png") . " " . __("Borrar"), 
					    					array('action' => 'delete', $student['Student']['id'], 'admin' => true), 
					    					array('escape' => false, 'confirm' => 'Está seguro?')); 
					     ?>
					</td>-->
				</tr>
			<?php endforeach; ?>
	 	</tbody>  
		</table>

		<ul class="pagination">
	    	<?php
	        	echo $this->Paginator->prev(__('anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'unavailable','disabledTag' => 'a'));
	            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'current','tag' => 'li','first' => 1));
	            echo $this->Paginator->next(__('siguiente'), array('tag' => 'li','currentClass' => 'unavailable'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	        ?>
	    </ul>
	</div>
	<?php endif; ?>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// Caching the movieName textbox:
		var name           = $('#names');
		var first_surname  = $('#first_surname');
		var second_surname = $('#second_surname');
		
		// Using jQuery UI's autocomplete widget:
		name.autocomplete({
			minLength    : 1,
			source       : '<?php echo $this->Html->url('/'); ?>' + 'admin/students/search_name?state=' + getCheckedRadioId('state'), // POST: data[Student][state]
			select: function(event, ui) {
	        }
		});

		first_surname.autocomplete({
			minLength    : 1,
			source       : '<?php echo $this->Html->url('/'); ?>' + 'admin/students/search_first_surname?state=' + getCheckedRadioId('state'), // POST: data[Student][state]
			select: function(event, ui) {
	        }
		});

		second_surname.autocomplete({
			minLength    : 1,
			source       : '<?php echo $this->Html->url('/'); ?>' + 'admin/students/search_second_surname?state=' + getCheckedRadioId('state'), // POST: data[Student][state]
			select: function(event, ui) {
	        }
		});
	});
	
	$('#names').keyup(function (e) {
	    if(e.keyCode == 13) {
	        e.preventDefault();
			$('StudentAdminIndexForm').submit();
	    }
	});

	$('#first_surname').keyup(function (e) {
	    if(e.keyCode == 13) {
	        e.preventDefault();
			$('StudentAdminIndexForm').submit();
	    }
	});

	$('#second_surname').keyup(function (e) {
	    if(e.keyCode == 13) {
	        e.preventDefault();
			$('StudentAdminIndexForm').submit();
	    }
	});

	function getCheckedRadioId(name) {
	    var elements = document.getElementsByName(name);

	    for (var i=0, len=elements.length; i<len; ++i)
	        if (elements[i].checked) return elements[i].value;
	}
</script>

