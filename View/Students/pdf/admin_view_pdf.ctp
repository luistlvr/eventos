<h1 align="center"><?php echo$student['Student']['fullname']; ?></h1>
<table class="table" width="100%" border="1" cellspacing="0">
<thead>
	<tr>
		<th width="150"><?php echo __('Título'); ?></th>
		<th width="50"><?php echo __('Fecha'); ?></th>
		<th width="50"><?php echo __('Creditos'); ?></th>
	</tr>
</thead>
<tbody>
<?php
	$credits = 0;
	foreach($student['Event'] as $event): ?>
		<tr>
			<td><?php echo $event['title']; ?></td>
			<td align="center"><?php echo date('Y-m-d h:i A', strtotime($event['datetime'])); ?></td>
			<td align="center"><?php echo $event['credits']; $credits += $event['credits']; ?></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td><b>Total</b></td>
		<td></td>
		<td align="center"><?php  echo $credits; ?></td>
	</tr>
</tbody>  
</table>
	
