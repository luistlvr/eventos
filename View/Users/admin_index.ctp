</br>

<div class="large-10 columns">
	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-6 columns">
		<?php 
			echo $this->Form->create('User');
			echo $this->Form->input('username_search', array('type' => 'text', 'placeholder' => __('Buscar'), 'label' => false, 'id' => 'username_search'));
		?>
	</div>

	<div class="large-2 columns">
		<?php 
			echo $this->Form->submit(__('Buscar'), array('name' => 'ok', 'div' => false, 'class' => 'button small success', 'id' => 'search')); 
			echo $this->Form->end();
		?>
	</div>

	<div class="large-4 columns" align="right">
		<?php echo $this->Html->link(
						$this->Html->image("agregar2_ico.png") . " " . __("Agregar Usuario"), 
						array('action' => 'add', 'admin' => true), 
						array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)); ?>
	</div>

	<div class="large-12 columns">
		<table width="100%">
		<thead>
			<tr>
				<th width="200"><?php echo $this->Paginator->sort('username', __('Usuario')); ?></th>
				<th width="400"><?php echo $this->Paginator->sort('password', __('Contraseña')); ?></th>
				<th width="250"><?php echo __("Acciones"); ?></th>
		    </tr>
		</thead>
		<tbody>
		    <?php foreach($users as $user): ?>
			    <tr>
			    	<td><?php echo $user['User']['username']; ?></td>
			        <td><?php echo $user['User']['password']; ?></td>
			        <td>
				        <?php
				        	echo $this->Html->link(
					    					$this->Html->image("edit-find.png") . " " . __("Ver"), 
					    					array('action' => 'view', $user['User']['id'], 'admin' => true), 
					    					array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;'; 

				        	echo $this->Html->link(
					    					$this->Html->image("edit.png") . " " . __("Editar"), 
					    					array('action' => 'edit', $user['User']['id'], 'admin' => true), 
					    					array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)). '&nbsp;&nbsp;'; 

				        	echo $this->Form->postLink(
					    					$this->Html->image("delete.png") . " " . __("Borrar"), 
					    					array('action' => 'delete', $user['User']['id'], 'admin' => true), 
					    					array('escape' => false, 'confirm' => 'Está seguro?')); 
				        ?>
			        </td>
			    </tr>
			<?php endforeach; ?>
		</tbody>
		</table>

		<ul class="pagination">
	    	<?php
	        	echo $this->Paginator->prev(__('anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'unavailable','disabledTag' => 'a'));
	            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'current','tag' => 'li','first' => 1));
	            echo $this->Paginator->next(__('siguiente'), array('tag' => 'li','currentClass' => 'unavailable'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	        ?>
	    </ul>
	</div>
</div>

<?php
	$jscript = "$(document).ready(function() {
					// Caching the movieName textbox:
					var username = $('#username_search');
					
					// Using jQuery UI's autocomplete widget:
					username.autocomplete({
						minLength    : 1,
						source       : '/eventos/admin/users/index',
						select: function(event, ui) {
				        }
					});
				});
				
				$('#username_search').keyup(function (e) {
				    if(e.keyCode == 13) {
				        e.preventDefault();
        				$('UserAdminIndexForm').submit();
				    }
				});";

	echo $this->Html->scriptBlock($jscript, array('inline'=>false));
?>