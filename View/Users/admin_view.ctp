</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<p><b><?php echo __('Usuario: ') . '</b>';   echo $user['User']['username']; ?></p>
		<p><b><?php echo __('Contraseña: '). '</b>'; echo $user['User']['password']; ?></b></p>
		<p><b><?php echo __('Rol: '). '</b>';        echo $user['User']['role']; ?></b></p>
		<p><b><?php echo __('Creado: '). '</b>';     echo $user['User']['created']; ?></b></p>
		<p><b><?php echo __('Modificado: '). '</b>'; echo $user['User']['modified']; ?></b></p>
	</div>

	<div class="large-12 columns"> 
	<?php
    	echo $this->Html->link(
    					$this->Html->image("edit.png") . " " . __("Editar"), 
    					array('action' => 'edit', $user['User']['id'], 'admin' => true), 
    					array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)). '&nbsp;&nbsp;'; 

    	echo $this->Form->postLink(
    					$this->Html->image("delete.png") . " " . __("Borrar"), 
    					array('action' => 'delete', $user['User']['id'], 'admin' => true), 
    					array('escape' => false, 'confirm' => 'Está seguro?')); 
    ?>
	</div>

	<div class="large-12 columns" style="padding-top: 10px;"> 
		<?php
			echo $this->Html->link(__('Volver', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary')); 
		?>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>
