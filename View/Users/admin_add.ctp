</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<?php 
			echo $this->Form->create('User', array('class' => 'custom')); 
			echo $this->Form->input('username', array('label' => '<b>'.__('Usuario*').'</b>', 'placeholder' => __('Usuario')));
			echo $this->Form->input('password', array('label' => '<b>'.__('Contraseña*').'</b>', 'placeholder' => __('Contraseña')));
			echo $this->Form->input('role', array('label' => '<b>'.__('Rol*').'</b>', 'options' => array('admin' => 'Administrador'), 'class' => 'medium'));
			echo $this->Form->submit(__('Agregar', true), array('name' => 'ok', 'div' => false, 'class' => 'button', 'id' => 'load')) . '&nbsp;&nbsp;';
			echo $this->Html->link(__('Cancelar', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
			echo $this->Form->end(); 
		?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

	<?php 
		if($isAjax) :
			echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
	?>
		<script>
			$(document).foundation();
		</script>

		<a class="close-reveal-modal">&#215;</a>
	<?php endif; ?>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<script type="text/javascript">
	$('#load').on('click', function() {
		var myForm = document.getElementById("UserAdminAddForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>