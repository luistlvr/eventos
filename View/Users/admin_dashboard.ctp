<div class="large-10 columns">
    <div class="large-12 columns">
        <h3><?php echo __('Bienvenido(a) al Panel de Administración'); ?></h3>
    </div>
    <hr>

    <div class="large-4 columns">
        <div class="panel radius">
            <b><?php echo __('Eventos'); ?></b>
            <hr>
            <?php
                echo $this->Html->link(
                        $this->Html->image("agregar2_ico.png") . " " . __("Agregar"), 
                        array('controller' => 'events', 'action' => 'add', 'admin' => true), 
                        array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;';  

                echo $this->Html->link(
                        $this->Html->image("home_examinar.png") . " " . __("Índice"), 
                        array('controller' => 'events', 'action' => 'index', 'admin' => true), 
                        array('escape' => false)); 
            ?>
        </div>
    </div>

    <div class="large-4 columns">
        <div class="panel radius">
            <b><?php echo __('Estudiantes'); ?></b>
            <hr>
                <?php 
                    echo $this->Html->link(
                            $this->Html->image("agregar2_ico.png") . " " . __("Agregar"), 
                            array('controller' => 'students', 'action' => 'add', 'admin' => true), 
                            array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;'; 

                    echo $this->Html->link(
                            $this->Html->image("home_examinar.png") . " " . __("Índice"), 
                            array('controller' => 'students', 'action' => 'index', 'admin' => true), 
                            array('escape' => false));
                ?>
        </div>
    </div>

    <div class="large-4 columns">
        <div class="panel radius">
            <b><?php echo __('Lugares'); ?></b>
            <hr>
                <?php 
                    echo $this->Html->link(
                            $this->Html->image("agregar2_ico.png") . " " . __("Agregar"), 
                            array('controller' => 'places', 'action' => 'add', 'admin' => true), 
                            array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;'; 

                    echo $this->Html->link(
                            $this->Html->image("home_examinar.png") . " " . __("Índice"), 
                            array('controller' => 'places', 'action' => 'index', 'admin' => true), 
                            array('escape' => false));
                ?>
        </div>
    </div>



    <div class="large-4 columns">
    </div>
</div>