<div class="row" style="padding-top: 25px;">
    <div class="small-6 small-centered columns">
        <div class="users form">
            <?php 
            	echo $this->Session->flash('auth', array("element" => "flash_error"));
            	echo $this->Session->flash('flash');   
            	echo $this->Form->create('User');
            ?>

            <fieldset>
                <legend><?php echo __('Bienvenido. Por favor ingrese su usuario y contraseña', true); ?></legend>
                <?php 
                    echo $this->Form->input('username', array('label' => __('Usuario', true)));
                    echo $this->Form->input('password',  array('label' => __('Contraseña', true)));
            	?>
            </fieldset>
            <?php echo $this->Form->end(array('label' => __('Acceder', true), 'class' => 'button')); ?>
        </div>
    </div>
</div>