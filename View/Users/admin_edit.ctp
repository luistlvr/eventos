</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<?php 
			echo $this->Form->create('User');
			echo $this->Form->input('User.id', array('type' => 'hidden'));
			echo $this->Form->input('User.username', array('label' => '<b>'.__('Nombre de Usuario*').'</b>', 'placeholder' => __('Nombre de Usuario')));
			echo $this->Form->input('User.password', array('type' => 'password', 'label' => '<b>'.__('Contraseña Nueva*').'</b>', 'placeholder' => __('Contraseña Nueva')));

			echo $this->Form->submit(__('Actualizar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
			echo $this->Html->link(__('Cancelar', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
			echo $this->Form->end(); 
		?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<script type="text/javascript">
	$('#load').on('click', function() {
		var myForm = document.getElementById("UserAdminEditForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>