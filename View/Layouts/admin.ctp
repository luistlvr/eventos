<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$webDescription = __('Administrador');
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Foundation 4 for IE 9 and earlier -->
	<!--[if gt IE 8]><!-->
	<link rel="stylesheet" href="/css/foundation4/normalize.css">
	<link rel="stylesheet" href="/css/foundation4/foundation.css">
	<!--<![endif]-->
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $webDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('normalize');
		echo $this->Html->css('foundation');
		echo $this->Html->css('autocomplete');
		echo $this->Html->css('layout');
		echo $this->Html->css('jquery.timepicker');
		echo $this->Html->css('foundation-datepicker');
		echo $this->Html->css('http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css');
		echo $this->Html->css('http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome-ie7.css');
		echo $this->Html->css('uploadify');

		echo $this->Html->script('vendor/custom.modernizr.js');
		echo $this->Html->script('vendor/jquery.js');
		echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js');
		echo $this->Html->script('jquery.uploadify.min.js');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="wrapper">
		<div id="container">
			<?php echo $this->element('modal'); ?>
			<div id="header"  class="row" style=" padding-top: 30px;">
				<div class="large-2 columns">
					<?php echo $this->Html->image('admin-panel.png'); ?>
				</div>
				<div class="large-10 columns">
					<h1 style="font-family: Serif;"><?php echo __("Inscripciones a eventos"); ?></h1>
				</div>
			</div>
			
			<?php if($logged_in) { ?>
				<nav class="top-bar">
					<ul class="title-area">
					    <li class="name">
					    	<h1><a href="#"><?php echo __("Usuario: ") . $current_user['username'] ?></a></h1>
					    </li>
				    </ul>

				    <section class="top-bar-section">
				    	<ul class="right">
				    		<li class="divider"></li>
				    		<li><?php echo $this->Html->link(
				    					$this->Html->image("home.gif") . " " . __("Inicio"), 
				    					array('controller' => 'users', 'action' => 'dashboard', 'admin' => true), 
				    					array('escape' => false)); ?></li>

				    		<li class="divider"></li>
				    		<li><?php echo $this->Html->link(
				    					$this->Html->image("user.gif") . " " . __("Usuarios"), 
				    					array('controller' => 'users', 'action' => 'index', 'admin' => true), 
				    					array('escape' => false)); ?></li>

				    		<li class="divider"></li>
				    		<li><?php echo $this->Html->link(
				    					$this->Html->image("logout.gif") . " " . __("Cerrar Sesión"), 
				    					array('controller' => 'users', 'action' => 'logout', 'admin' => true), 
				    					array('escape' => false)); ?></li>
				    	</ul>
				    </section>
				</nav>
			<?php } else { ?>
				<hr>
			<?php } ?>

			<div id="content" class="row">
				<?php 
					if($logged_in) : 
						echo $this->element('menu/nav_bar');
				 	endif; 
				 ?>

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
		<div class="push"></div>
	</div>

	<a href="#" class="scrollup">Scroll</a>

	<div class="footer">
		<hr>
		<div class="row">
			<div class="small-12 columns">
				<a class="regular" href="http://academico.ucsp.edu.pe">Universidad Católica San Pablo</a>
				<p class="copyright">Derechos reservados © 2013 - Powered by Bytegolem</p>
			</div>
		</div>
	</div>

	<?php
		echo $this->Html->script('foundation/foundation.js');
		echo $this->Html->script('foundation/foundation.alerts.js');
		echo $this->Html->script('foundation/foundation.reveal.js');
		echo $this->Html->script('foundation/foundation.forms.js');
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
		echo $this->Html->script('jquery.timepicker.js');
		echo $this->Html->script('foundation/foundation-datepicker.js');
		echo $this->Html->script('datepair.js');
		
		echo $this->Js->writeBuffer();
	?>

	<script>
		$(document).foundation();

		$(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
 
    });
	</script>

</body>
	<!-- Foundation 4 for IE 9 and later -->
	<!--[if gt IE 8]><!-->
    <script src="/js/foundation4/foundation.min.js"></script>
    <script>
        $(document).foundation();
    </script>
	<!--<![endif]-->
</html>
