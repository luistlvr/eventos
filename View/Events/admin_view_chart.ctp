<?php if($isAjax): ?>
	<a class="close-reveal-modal">&#215;</a>
<?php else: ?>
	<div class="large-2 columns">
	    <ul class="side-nav">
	        <li class="name"><h3><?php echo __("Menú"); ?></h3></li>
	        <li class="divider"></li>
	        <li class="active">
	        	<?php echo $this->Html->link(__('Eventos'), array('controller' => 'events', 'action' => 'index', 'admin' => true),	array('escape' => false)); ?>
	        </li>
	        <li class="divider"></li>
	        <li>
	        	<?php echo $this->Html->link(__('Estudiantes'), array('controller' => 'students', 'action' => 'index', 'admin' => true), array('escape' => false)); ?>
	        </li>
	        <li class="divider"></li>
	    </ul>
	</div>
	</br>
<?php endif; ?>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
	<?php 
		echo $this->GoogleChart->create()
	    ->setTitle($event['Event']['title'], array('size' => 15, 'color' => '000000'))
	    ->setType('pie', array('3d'))
	    ->setSize(600, 300)
	    ->setMargins(10, 10, 10, 10)
	    ->addData($semester_values)
	    ->setPieChartLabels($semester_labels); 
	?>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>