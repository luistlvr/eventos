</br>
<?php 
	echo $this->Form->create('Event', array('class' => 'custom', 'type' => 'file'));
	echo $this->Form->input('Event.id', array('type' => 'hidden'));
?>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('title', array('label' => '<b>'.__('Título*', true).'</b>', 'placeholder' => __('Título', true))); ?>	
	</div>

	<div class="large-12 columns">
		<?php 
			echo $this->Form->input('description', array('label' => '<b>'.__('Descripción').'</b>', 'placeholder' => __('Descripción'), 'type' => 'textarea')); 
		?>	
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->input('speaker', array('label' => '<b>'. __('Ponente', true).'</b>', 'placeholder' => __('Ponente', true))); ?>	
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->input('date', array('label' => '<b>'.__('Fecha*', true).'</b>', 'placeholder' => __('Fecha', true), 'type' => 'text', 'id' => 'dp', 'class' => 'span2')); ?>
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->input('credits', array('label' => '<b>'.__('Créditos*', true).'</b>', 'placeholder' => __('Créditos', true), 'type'=>'select', 'options' => $credit_options)); ?>
	</div>

	<div class="large-6 columns">
	<?php echo $this->Form->input('type', array('label' => '<b>'.__('Tipo*', true).'</b>', 'placeholder' => __('Tipo', true), 'type'=>'select','options' => $type_options)); ?>
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->input('place', array('label' => '<b>'.__('Lugar*', true).'</b>', 'placeholder' => __('Lugar', true))); ?>
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->input('capacity', array('label' => '<b>'.__('Capacidad*', true).'</b>', 'placeholder' => __('Capacidad', true))); ?>
	</div>

	<div class="large-12 columns">
		<p class="datepair" data-language="javascript">
		<?php 
			echo $this->Form->input('start_time', array('label' =>  '<b>'.__('Hora de Inicio*').'</b>', 'placeholder' => __('Hora de Inicio', true), 'type' => 'text', 'div' => false, 'class' => 'time start', 'before' => '<span style="display:inline-block; width: 47%;">', 'after' => '</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')); 
			echo $this->Form->input('end_time', array('label' => '<b>'.__('Hora de Fin*').'</b>', 'placeholder' => __('Hora de Fin', true), 'type' => 'text', 'div' => false, 'class' => 'time end', 'before' => '<span style="display:inline-block; width: 47%;">', 'after' => '</span>')); 
		?>
		</p>
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->radio('state', array('0' => '&nbsp;Inactivo<br><br>', '1' => '&nbsp;Activo<br><br>', '2' => '&nbsp;Lista Tomada'), 
		array('label'  => false, 'legend' => __('Estado'))); ?>
	</div>

	<div class="large-6 columns">
		<?php echo $this->Form->radio('punishment', array('0' => '&nbsp;Sin Penalidad<br><br>', '1' => '&nbsp;Con Penalidad'), array('label'  => false, 'legend' => __('Penalidad')));  ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('file', array('type'=>'file', 'label' => false, 'class' => 'tiny button radius secondary')); ?>
	</div>

	<div class="large-12 columns">
		<?php 
			echo $this->Form->submit(__('Actualizar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
			echo $this->Html->link(__('Cancelar', true), array('action' => 'view', $this->request->data['Event']['id'], 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
			echo $this->Form->end(); 
		?>

		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if($isAjax): 
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
		echo $this->Html->script('datepair.js');
?>
	<script>
		$(document).foundation();
	</script>

	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-events").addClass('active');
	});

	$(function () {
		window.prettyPrint && prettyPrint();
		$('#dp').fdatepicker({
			format: 'yyyy-mm-dd'
		});
	});

	$('#load').on('click', function() {
		var myForm = document.getElementById("EventAdminEditForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});
</script>
