</br>

<div class="large-10 columns">
	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-6 columns">
		<?php 
			echo $this->Form->create('Event', array('class' => 'custom', 'type' => 'get'));
			echo $this->Form->hidden('change_flag', array('value' => true));
			echo $this->Form->input('title_search', array('type' => 'text', 'placeholder' => __('Buscar'), 'label' => false, 'id' => 'title_search'));
		?>
	</div>

	<div class="large-2 columns">
		<?php 
			echo $this->Form->submit(__('Buscar'), array('name' => 'ok', 'div' => false, 'class' => 'button small success', 'id' => 'search')); 
		?>
	</div>

	<div class="large-4 columns" align="right">
		<?php echo $this->Html->link(
						$this->Html->image("agregar2_ico.png") . " " . __("Agregar Evento"), 
						array('action' => 'add', 'admin' => true), 
						array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)); ?>
	</div>

	<div class="large-12 columns">
		<div class="inline_labels">
		<?php 
			echo $this->Form->radio('state', array('0' => 'Inactivos', '1' => 'Activos', '2' => 'Lista Tomada', '3' => 'Todos'), array('legend' => false, 'div' => false, 'default' => '3'/*, 'onclick' => 'this.form.submit();'*/)); 
			echo $this->Form->end();
		?>
		</div>
	</div>

	<div class="large-12 columns">
		<table width="100%">
		<thead>
			<tr>
				<th width="300"><?php echo $this->Paginator->sort('title', __('Título', true)); ?></th>
				<th width="250"><?php echo $this->Paginator->sort('speaker', __('Ponente', true)); ?></th>
				<th width="150"><?php echo $this->Paginator->sort('place', __('Lugar', true)); ?></th>
				<th width="150"><?php echo $this->Paginator->sort('type', __('Tipo', true)); ?></th>
				<th width="250"><?php echo $this->Paginator->sort('datetime', __('Fecha', true)); ?></th>
				<!-- <th width="500"><?php echo __("Acciones"); ?></th> -->
		    </tr>
		</thead>
		<tbody>
		    <?php foreach ($events as $event): ?>
			    <tr>
			    	<!-- <td><?php echo $this->Html->link($event['Event']['title'], 
					    					array('action' => 'view_chart', $event['Event']['id'], 'admin' => true), 
					    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')) . '&nbsp;&nbsp;'; ?></td> -->
					<td><?php echo $this->Html->link($event['Event']['title'], 
					    				array('action' => 'view', $event['Event']['id'], 'admin' => true), 
					    				array('escape' => false,/*'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true',*/ 'title' => 'Ver'));  ?></td>
					<td><?php echo $event['Event']['speaker']; ?></td>
					<td><?php echo $event['Event']['place']; ?></td>
					<td><?php echo $type_options[$event['Event']['type']]; ?></td>
			        <td><?php echo date('Y-m-d h:i A', strtotime($event['Event']['datetime'])); ?></td>
			        <!-- <td>
			        	<?php
				        	echo $this->Html->link(
					    					$this->Html->image("edit-find.png") . " " . __("Ver"), 
					    					array('action' => 'view', $event['Event']['id'], 'admin' => true), 
					    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')) . '&nbsp;&nbsp;'; 

				        	echo $this->Html->link(
					    					$this->Html->image("edit.png") . " " . __("Editar"), 
					    					array('action' => 'edit', $event['Event']['id'], 'admin' => true), 
					    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')). '&nbsp;&nbsp;'; 

				        	echo $this->Form->postLink(
					    					$this->Html->image("delete.png") . " " . __("Borrar"), 
					    					array('action' => 'delete', $event['Event']['id'], 'admin' => true), 
					    					array('escape' => false, 'confirm' => 'Está seguro?')) . '&nbsp;&nbsp;'; 

				        	echo $this->Html->link($this->Html->image("message.png") . " " . __('Mensaje'),
	               							array('action' => 'send_mail', $event['Event']['id'], 'admin' => true),
	        								array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')) . '&nbsp;&nbsp;';

				        	echo $this->Html->link($this->Html->image("pdf.png") . " " . __("Pdf"), 
							        		array('action' => 'view_pdf', 'ext' => 'pdf', $event['Event']['id'], 'admin' => true),
							        		array('escape' => false, 'target' => '_blank')) . '&nbsp;&nbsp;'; 	

				        	echo $this->Html->link($this->Html->image("take-list.png") . " " . __("Lista"),
							               	array('action' => 'take_list', $event['Event']['id'], 'admin' => true),
							        		array('escape' => false));
				        ?>
			        </td> -->
			    </tr>
			<?php endforeach; ?>
		</tbody>
		</table>

		<ul class="pagination">
	    	<?php
	        	echo $this->Paginator->prev(__('anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'unavailable','disabledTag' => 'a'));
	            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'current','tag' => 'li','first' => 1));
	            echo $this->Paginator->next(__('siguiente'), array('tag' => 'li','currentClass' => 'unavailable'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	        ?>
	    </ul>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// Caching the movieName textbox:
		var title = $('#title_search');

		// Using jQuery UI's autocomplete widget:
		title.autocomplete({
			minLength    : 1,
			source       : '<?php echo $this->Html->url('/'); ?>' + 'admin/events/index?state=' + getCheckedRadioId('state'), // POST: data[Event][state]
			select: function(event, ui) {
	        }
		});
	});
	
	$('#title_search').keyup(function (e) {
	    if(e.keyCode == 13) {
	        e.preventDefault();
			$('EventAdminIndexForm').submit();
	    }
	});

	function getCheckedRadioId(name) {
	    var elements = document.getElementsByName(name);

	    for (var i=0, len=elements.length; i<len; ++i)
	        if (elements[i].checked) return elements[i].value;
	}
</script>