<div class="large-12 columns">
	<?php echo $this->Session->flash("flash"); ?>
</div>

<div class="large-4 columns">
	<?php 
		echo $this->Form->create('Event', array('class' => 'custom', 'type' => 'get'));
		echo $this->Form->hidden('change_flag', array('value' => true));
		echo $this->Form->input('title_search', array('type' => 'text', 'placeholder' => __('Buscar'), 'label' => false, 'id' => 'title_search'));
	?>
</div>

<div class="large-2 columns">
	<?php 
		echo $this->Form->submit(__('Buscar'), array('name' => 'ok', 'div' => false, 'class' => 'button small success', 'id' => 'search')); 
	?>
</div>

<div class="large-12 columns">
	<div class="inline_labels">
	<?php 
		echo $this->Form->radio('punishment', array('0' => 'Sin Penalidad', '1' => 'Con Penalidad', '3'  => 'Todos'), array('legend' => false, 'div' => false, 'default' => '3', 'onclick' => 'this.form.submit();')); 
		echo $this->Form->end();
	?>
	</div>
</div>

<div class="large-12 columns">
	<table width="100%">
	<thead>
		<tr>
	  		<th width="100"></th>
			<th width="500"><?php echo $this->Paginator->sort('title', __('Título', true)); ?></th>
			<th width="200"><?php echo $this->Paginator->sort('date', __('Fecha', true)); ?></th>
			<th width="50"><?php echo $this->Paginator->sort('credits', __('Créditos')); ?></th>
			<th width="100"></th>
	    </tr>
	</thead>
	<tbody>
	    <?php foreach ($events as $event): ?>
		    <tr>
		    	<td>
		    		<?php
		    			if(!is_null($event['Event']['image'])) { 
	                    	echo $this->Html->image('uploads/events/' . $event['Event']['image'], array('url' => '/' . IMAGES_URL . 'uploads/events/' . $event['Event']['image'], 'width'=>'70px', 'height'=>'70px', 'class' => 'th radius')); 
		    			} 
		    		?>
		    	</td>
		    	<td>
		            <?php echo $this->Html->link($event['Event']['title'], 
							array('action' => 'view', $event['Event']['id']), 
							array('escape' => false, 'title' => $event['Event']['title'], 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'));
		            ?>
		        </td>
		        <td><?php echo date('Y-m-d h:i A', strtotime($event['Event']['datetime'])); ?></td>
		        <td align="middle"><?php echo $event['Event']['credits']; ?></td>
		        <td style="padding-top: 20px;">
			        <?php
			        	if($event['Event']['inscriptions_count'] < $event['Event']['capacity']) {
				        	echo $this->Html->link(__('Inscribirse'),
					            array('action' => 'subscribe', $event['Event']['id']),
					        	array('escape' => false, 'class' => 'small button', 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'));
				        } else {
				        	echo '<span data-tooltip class="has-tip" title="Aún puedes asistir al evento en caso alguien falte.">';
				        	echo $this->Html->link(__('Cerrado'), '#', array('escape' => false, 'class' => 'small button alert'));
				        	echo '</span>';
				        }
			        ?>
		        </td>
		    </tr>
		<?php endforeach; ?>
	</tbody>
	</table>

	<ul class="pagination">
    	<?php
        	echo $this->Paginator->prev(__('anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'unavailable','disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'current','tag' => 'li','first' => 1));
            echo $this->Paginator->next(__('siguiente'), array('tag' => 'li','currentClass' => 'unavailable'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
        ?>
    </ul>
</div>

<?php
	$jscript = "$(document).ready(function() {
					// Caching the movieName textbox:
					var title = $('#title_search');
					
					// Using jQuery UI's autocomplete widget:
					title.autocomplete({
						minLength    : 1,
						source       : '/eventos/events/index?punishment=' + getCheckedRadioId('punishment'), // POST: data[Event][punishment]
						select: function(event, ui) {
				        }
					});
				});
				
				$('#title_search').keyup(function (e) {
				    if(e.keyCode == 13) {
				        e.preventDefault();
        				$('EventIndexForm').submit();
				    }
				});

				function getCheckedRadioId(name) {
				    var elements = document.getElementsByName(name);

				    for (var i=0, len=elements.length; i<len; ++i)
				        if (elements[i].checked) return elements[i].value;
				}";

	echo $this->Html->scriptBlock($jscript, array('inline'=>false));
?>