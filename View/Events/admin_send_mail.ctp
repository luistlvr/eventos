<?php
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>

<?php if($isAjax): ?>
	<?php 
		echo $this->Html->script('jquery.uploadify.min.js');
		echo $this->Html->css('uploadify');
	?>
	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
        <h3><?php echo $event['Event']['title']; ?></h3>
    </div>

    <div class="large-12 columns">
    	<p><b><?php echo __('Lugar: ', true); ?></b><?php echo $event['Event']['place']; ?>&nbsp;&nbsp;&nbsp;
    	<b><?php echo __('Fecha: ', true); ?></b><?php echo h($dias[date('w', strtotime($event['Event']['date']))].", ".date('d', strtotime($event['Event']['date']))." de ".$meses[date('n', strtotime($event['Event']['date']))-1]. " del ".date('Y', strtotime($event['Event']['date']))); ?></b>&nbsp;&nbsp;&nbsp;
    	<b><?php echo __('Crédito(s): ', true); ?></b><?php echo $event['Event']['credits']; ?>&nbsp;&nbsp;&nbsp;
    	<?php 
			if(!empty($event['Event']['speaker'])) {
				echo "<b>".__('Ponente: ', true)."</b>";
				echo $event['Event']['speaker'] . '</p>'; 
			} else {
				echo '</p>';
			}
		?>
	</div>

    <hr>

	<?php echo $this->Form->create('Message', array('type' => 'file')); ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('title', array('label' => '<b>'.__('Título', true).'</b>', 'placeholder' => __('Título', true))); ?>
	</div>

	<div class="large-12 columns">
		<?php echo $this->Form->input('message', array('label' => '<b>'.__('Mensaje', true).'</b>', 'placeholder' => __('Mensaje', true), 'type' => 'textarea')); ?>
	</div>

	<div class="large-12 columns" style="padding-top: 10px;">
		<?php echo __("1 Archivo Max."); ?>
		<div id="queue"></div>
		<?php 
			echo $this->Form->input('file', array('type'=>'file', 'label' => false, 'name' => 'file_upload', 'id' => 'file_upload'));
			echo $this->Form->input('attach', array('type'=>'hidden', 'id' => 'file')); 
		?>
		<p><b id="attach"></b></p>
	</div>

	<div class="large-12 columns">
	<?php
		echo $this->Form->submit(__('Enviar', true), array('name' => 'ok', 'class' => 'button', 'id' => 'load', 'div' => false)) . '&nbsp;&nbsp;';
		echo $this->Html->link(__('Cancelar', true), array('action' => 'view', $id, 'admin' => true), array('escape' => false, 'class' => 'button secondary'));
		echo $this->Form->end(); 
	?>
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php echo $this->Html->script('tinymce.min.js'); ?>
<script type="text/javascript">  
	$(document).ready(function() {
		$("#manage-events").addClass('active');
	});

  $('#load').on('click', function() {
		var myForm = document.getElementById("MessageAdminSendMailForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
		} 
	});

  <?php $timestamp = time(); ?>
  var path = "<?= $this->Html->url('/app/webroot/uploadify/'); ?>";

  $(function() {
        $('#file_upload').uploadify({
        	'formData' : {
        		'timestamp' : '<?php echo $timestamp; ?>',
        		'token'     : '<?php echo md5('unique_salt' . $timestamp); ?>'
        	},
        	'fileSizeLimit' : '10MB',
            'swf'      : path + 'uploadify.swf',
            'uploader' : path + 'uploadify.php',
            'buttonText' : '<?php echo __('Adjuntar'); ?>',
            'onUploadComplete' : function(file, errorCode, errorMsg, errorString) {
	            document.getElementById("attach").innerHTML = file['name'];
	            $('#file').val(file['name']);
	        } 
        });
    }); 

  	tinymce.init({
    	selector: "textarea"
 	});
</script>

