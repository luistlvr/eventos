<?php
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>
	
<div class="large-12 columns">
	<p><b><?php echo __('Fecha: ', true); echo h($dias[date('w', strtotime($event['Event']['date']))].", ".date('d', strtotime($event['Event']['date']))." de ".$meses[date('n', strtotime($event['Event']['date']))-1]. " del ".date('Y', strtotime($event['Event']['date']))); ?></b></p>
</div>
<div class="large-6 columns">
	<p><b><?php echo __('Hora de Inicio: ', true); echo h(date('h:i A', strtotime($event['Event']['start_time']))); ?></b></p>
</div>
<div class="large-6 columns">
	<p><b><?php echo __('Hora de Fin: ', true); echo h(date('h:i A', strtotime($event['Event']['end_time']))); ?></b></p>
</div>
<hr>

<div class="large-7 columns">
		<table style="border: none;">
			<tr>
			<?php 
				if(!empty($event['Event']['speaker'])) {
					echo "<td><font color='#08088A'>".__('Ponente: ', true)."</font></td>";
					echo "<td>" . $event['Event']['speaker'] . '</td>'; 
				}
			?>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Lugar: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['place']; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Tipo: ', true)."</font>"; ?></td>
			<td><?php echo $type_options[$event['Event']['type']]; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Crédito(s): ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['credits']; ?></td>
			</tr>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Capacidad: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['capacity']; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Inscritos: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['inscriptions_count']; ?></td>
			</tr>
		</table> 
	</div>

<div class="large-5 columns">
	<?php if(!empty($event['Event']['image'])) echo $this->Html->image('uploads/events/' . $event['Event']['image'], array('width'=>'200px', 'height'=>'200px',  'class' => "th")); ?>
</div>

<div class="large-12 columns">
<?php 
	if($event['Event']['inscriptions_count'] < $event['Event']['capacity']) {
		echo $this->Html->link(__('Inscribirse'),
		array('action' => 'subscribe', $event['Event']['id']),
	    array('escape' => false, 'class' => 'small button', 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'));
	} else {
	   	echo '<span data-tooltip class="has-tip" title="Aún puedes asistir al evento en caso alguien falte.">';
    	echo $this->Html->link(__('Cerrado'), '#', array('escape' => false, 'class' => 'small button alert'));
    	echo '</span>';
	}
?>
</div>

<?php if($isAjax): ?>
	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

<!-- Small Modal -->
<div id="secondModal" class="reveal-modal small">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>