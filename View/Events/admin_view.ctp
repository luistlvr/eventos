<?php
	$dias   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$punish = array("No", "Si");
?>

</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-12 columns">
		<p><b><?php echo __('Fecha: ', true); echo h($dias[date('w', strtotime($event['Event']['date']))].", ".date('d', strtotime($event['Event']['date']))." de ".$meses[date('n', strtotime($event['Event']['date']))-1]. " del ".date('Y', strtotime($event['Event']['date']))); ?></b></p>
	</div>

	<div class="large-6 columns">
		<p><b><?php echo __('Hora de Inicio: ', true); echo h(date('h:i A', strtotime($event['Event']['start_time']))); ?></b></p>
		<hr>
	</div>

	<div class="large-6 columns">
		<p><b><?php echo __('Hora de Fin: ', true); echo h(date('h:i A', strtotime($event['Event']['end_time']))); ?></b></p>
		<hr>
	</div>

	<div class="large-7 columns">
		<table style="border: none;">
			<tr>
			<?php 
				if(!empty($event['Event']['speaker'])) {
					echo "<td><font color='#08088A'>".__('Ponente: ', true)."</font></td>";
					echo "<td>" . $event['Event']['speaker'] . '</td>'; 
				}
			?>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Lugar: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['place']; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Tipo: ', true)."</font>"; ?></td>
			<td><?php echo $type_options[$event['Event']['type']]; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Crédito(s): ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['credits']; ?></td>
			</tr>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Capacidad: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['capacity']; ?></td>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Inscritos: ', true)."</font>"; ?></td>
			<td><?php echo $event['Event']['inscriptions_count']; ?></td>
			</tr>
			</tr>
			<tr>
			<td><?php echo "<font color='#08088A'>".__('Penalidad: ', true)."</font>"; ?></td>
			<td><?php echo $punish[$event['Event']['punishment']]; ?></td>
			</tr>
		</table> 
	</div>

	<div class="large-5 columns">
		<?php if(!empty($event['Event']['image'])) echo $this->Html->image('uploads/events/' . $event['Event']['image'], array('width'=>'200px', 'height'=>'200px',  'class' => "th")); ?>
	</div>
	
	<div class="large-12 columns"> 
	<?php
    	echo $this->Html->link(
    					$this->Html->image("edit.png") . " " . __("Editar"), 
    					array('action' => 'edit', $event['Event']['id'], 'admin' => true), 
    					array('escape' => false/*, 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'*/)). '&nbsp;&nbsp;'; 

    	echo $this->Form->postLink(
    					$this->Html->image("delete.png") . " " . __("Borrar"), 
    					array('action' => 'delete', $event['Event']['id'], 'admin' => true), 
    					array('escape' => false, 'confirm' => 'Está seguro?')) . '&nbsp;&nbsp;'; 

    	echo $this->Html->link($this->Html->image("message.png") . " " . __('Mensaje'),
							array('action' => 'send_mail', $event['Event']['id'], 'admin' => true),
						array('escape' => false/*, 'data-reveal-id' => 'secondModal', 'data-reveal-ajax' => 'true'*/)) . '&nbsp;&nbsp;';

    	echo $this->Html->link($this->Html->image("take-list.png") . " " . __("Lista"),
		               	array('action' => 'take_list', $event['Event']['id'], 'admin' => true),
		        		array('escape' => false)) . '&nbsp;&nbsp;';

    	echo $this->Html->link($this->Html->image("pdf.png") . " " . __("Pdf"), 
		        		array('action' => 'view_pdf', 'ext' => 'pdf', $event['Event']['id'], 'admin' => true),
		        		array('escape' => false, 'target' => '_blank')) . '&nbsp;&nbsp;'; 	

    	echo $this->Html->link($this->Html->image("excel.png") . " " . __("Excel"), 
		        		array('action' => 'view_excel', $event['Event']['id'], 'admin' => true),
		        		array('escape' => false)); 	
    ?>
	</div>
	
	<div class="large-12 columns" style="padding-top: 10px;"> 
		<?php
			echo $this->Html->link(__('Volver', true), array('action' => 'index', 'admin' => true), array('escape' => false, 'class' => 'button secondary')); 
		?>
	</div>

<!-- Small Modal -->
<div id="secondModal" class="reveal-modal small">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-events").addClass('active');
	});
</script>