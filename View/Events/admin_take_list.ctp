<?php
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>

</br>

<?php if(!$isAjax): ?>
	<div class="large-10 columns">
<?php endif; ?>

	<div class="large-12 columns">
		<?php echo $this->Session->flash("flash"); ?>
	</div>

	<div class="large-8 columns">
        <h3><?php echo $event['Event']['title']; ?></h3>
    </div>

	<div class="large-4 columns" align="right">
		<?php echo $this->Html->link(
						$this->Html->image("agregar_ico.png") . " " . __("Agregar Estudiante"), 
						array('action' => 'add_student', $event['Event']['id'], 'admin' => true), 
						array('escape' => false/*, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true'*/)); ?>
	</div>

	<div class="large-12 columns">
    	<p><b><?php echo __('Lugar: ', true); ?></b><?php echo $event['Event']['place']; ?>&nbsp;&nbsp;&nbsp;
    	<b><?php echo __('Fecha: ', true); ?></b><?php echo h($dias[date('w', strtotime($event['Event']['date']))].", ".date('d', strtotime($event['Event']['date']))." de ".$meses[date('n', strtotime($event['Event']['date']))-1]. " del ".date('Y', strtotime($event['Event']['date']))); ?></b>&nbsp;&nbsp;&nbsp;
    	<b><?php echo __('Crédito(s): ', true); ?></b><?php echo $event['Event']['credits']; ?>&nbsp;&nbsp;&nbsp;
    	<?php 
			if(!empty($event['Event']['speaker'])) {
				echo "<b>".__('Ponente: ', true)."</b>";
				echo $event['Event']['speaker'] . '</p>'; 
			} else {
				echo '</p>';
			}
		?>
	</div>

    <hr>

	<?php echo $this->Form->create(null, array('class' => 'custom')); ?>

	<div class="large-12 columns">
		<table width="100%">
		<thead>
			<tr>
				<th width="100"><?php echo __('Código'); ?></th>
				<th width="300"><?php echo __('Nombre'); ?></th>
				<th width="300"><?php echo __('Correo'); ?></th>
				<th width="100" style="text-align: center;"><?php echo __('Asistencia'); ?></th>
		    </tr>
		</thead>
		<tbody>
			<?php foreach($students as $student): ?>
				<tr>
					<td><?php echo $student['code']; ?></td>
					<td><?php echo $this->Html->link($student['fullname'], 
				    					array('controller' => 'students', 'action' => 'view', $student['id'], 'admin' => true), 
				    					array('escape' => false, 'data-reveal-id' => 'myModal', 'data-reveal-ajax' => 'true')); ?></td>
				   	<td><?php echo $student['email']; ?></td>
					<td style="text-align:center;">
						<?php 
							if($student['Inscription']['state'] == $absent) {
								echo $this->Form->input("relation".$student['Inscription']['id']."id", array('label' => false, 'type' => 'checkbox', 'value' => $student['Inscription']['id'].":".$student['id'], 'label' => false, 'multiple' => true, 'checked', 'hiddenField' => false));
							} else {
								echo $this->Form->input("relation".$student['Inscription']['id']."id", array('label' => false, 'type' => 'checkbox', 'value' => $student['Inscription']['id'].":".$student['id'], 'label' => false, 'multiple' => true, 'hiddenField' => false));
							}
						?>
					</td>
				</tr>
			 <?php endforeach; ?>
		 </tbody>  
		 </table>
	</div>

	<div class="large-12 columns">
		<div class="large-2 columns">
			<?php
			 	echo $this->Form->submit(__('Salvar', true), array('name' => 'ok', 'div' => false, 'class' => 'button', 'id' => 'load'));
			?> 
		</div>
		<div class="large-10 columns">
			<?php
			 	echo $this->Form->submit(__('Cancelar', true), array('name' => 'cancel', 'class' => 'button secondary'));
				echo $this->Form->end();
			?> 
		</div>
	
		<div id="spinner" class="preloader" style="display:none;"></div>
	</div>

<?php if(!$isAjax): ?>
	</div>
<?php endif; ?>

<?php if($isAjax): echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js'); ?>
	<script>
		$(document).foundation();
	</script>
<?php endif; ?>
	

<script type="text/javascript">
	$(document).ready(function() {
		$("#manage-events").addClass('active');
	});
</script>
