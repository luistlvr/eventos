<?php echo $this->Form->create('Student', array('class' => 'custom')); ?>

<div class="large-6 small-4 columns">
	<?php echo $this->Form->input('code', array('label' => '<b>'.__('Código', true).'</b>', 'placeholder' => __('Código', true))); ?>
</div>

<div class="large-6 small-8 columns">
	<?php echo $this->Form->input('names', array('label' => '<b>'.__('Nombres', true).'</b>', 'placeholder' => __('Nombres', true))); ?>
</div>

<div class="large-6 small-6 columns">
	<?php echo $this->Form->input('first_surname', array('label' => '<b>'.__('Apellido Paterno', true).'</b>', 'placeholder' => __('Apellido Paterno', true))); ?>
</div>

<div class="large-6 small-6 columns">
	<?php echo $this->Form->input('second_surname', array('label' => '<b>'.__('Apellido Materno', true).'</b>', 'placeholder' => __('Apellido Materno', true))); ?>
</div>

<div class="large-4 small-2 columns">
	<?php echo $this->Form->input('semester', array('label' => '<b>'.__('Semestre', true).'</b>', 'type'=>'select', 'options' => $semester_options, 'class' => 'medium')); ?>
</div>

<?php if($only_ucsp_email): ?>
	<div class="large-8 small-10 columns">
		<div class="row collapse">
			<label><b><?php echo __('Correo', true); ?></b></label>
			<div class="small-6 columns">
			<?php echo $this->Form->input('mail', array('label' => false, 'placeholder' => __('Correo', true))); ?>
			</div>
			<div class="small-6 columns">
			  <span class="postfix radius"><?php echo __("@ucsp.edu.pe") ?></span>
			</div>
		</div>
	</div>
<?php else: ?>
	<div class="large-8 small-10 columns">
		<?php echo $this->Form->input('email', array('label' => '<b>'.__('Correo', true).'</b>', 'placeholder' => __('Correo', true))); ?>
	</div>
<?php endif; ?>

<hr>
<div class="large-12 columns">
	<?php
		echo $this->Form->submit(__('Inscribirse', true), array('name' => 'ok', 'div' => false, 'class' => 'button', 'id' => 'load'));
		echo $this->Form->end(); 
	?>
	<div id="spinner" class="preloader" style="display:none;"></div>
</div>

<?php 
	if($isAjax):
		echo $this->Html->script('http://foundation.zurb.com/docs/assets/docs.js');
?>
	<script>
		$(document).foundation();
	</script>

	<a class="close-reveal-modal">&#215;</a>
<?php endif; ?>

<script type="text/javascript">
	$('#load').on('click', function() {
		var myForm = document.getElementById("StudentSubscribeForm");
		
		if(myForm.checkValidity()) {
			var $this = $(this);
			document.getElementById('spinner').style.display = 'block';
	   	 	//$this.button.setAttribute("class", "disabled");
		} 
	});
</script>