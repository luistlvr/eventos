<?php
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    header ("Expires: Mon, 28 Oct 2008 05:00:00 GMT");
    header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    header ("Content-type: application/vnd.ms-excel; charset=UTF-8");
    header ("Content-Disposition: attachment; filename=\"" . $event['title'] . ".xls" );
    header ("Content-Description: Generated Report" );
?>

<STYLE type="text/css">
  .tableTd {
      border-width: 0.5pt; 
      border: solid; 
  }
  .tableTdContent{
    border-width: 0.5pt; 
    border: solid;
  }
  #titles{
    font-weight: bolder;
  }
   
</STYLE>

<table>
    <tr>
        <td width="150"><b><?php echo mb_convert_encoding($event['title'], 'UTF-16LE', 'UTF-8'); ?><b></td>
    </tr>
    <tr>
        <td><b><?php echo mb_convert_encoding(__('Fecha de Exportación:'), 'UTF-16LE', 'UTF-8'); ?></b></td>
        <td><?php echo $meses[date('n')-1] . " " . date("j, Y, g:i a"); ?></td>
    </tr>
    <tr>
        <td><b><?php echo mb_convert_encoding($event['datetime'], 'UTF-16LE', 'UTF-8'); ?><b></td>
    </tr>
    <tr>
        <td><b><?php echo mb_convert_encoding($event['speaker'], 'UTF-16LE', 'UTF-8'); ?><b></td>
    </tr>

    <tr>
    </tr>
    <tr>
        <th width="150"><?php echo mb_convert_encoding(__('Nombre', true), 'UTF-16LE', 'UTF-8'); ?></th>
        <th width="150"><?php echo mb_convert_encoding(__('Email', true), 'UTF-16LE', 'UTF-8'); ?></th>
        <th width="100"><?php echo mb_convert_encoding(__('Código', true), 'UTF-16LE', 'UTF-8'); ?></th>
        <th width="100"><?php echo mb_convert_encoding(__('Firma', true), 'UTF-16LE', 'UTF-8'); ?></th>
    </tr>

    <?php foreach($students as $student): ?>
        <tr>
        <?php
            echo '<td class="tableTdContent">' . mb_convert_encoding($student['fullname'], 'UTF-16LE', 'UTF-8') . '</td>';
            echo '<td class="tableTdContent">' . mb_convert_encoding($student['email'], 'UTF-16LE', 'UTF-8') . '</td>';
            echo '<td class="tableTdContent">' . mb_convert_encoding($student['code'], 'UTF-16LE', 'UTF-8') .'</td>';
            echo '<td></td>';
        ?>
        </tr>
     <?php endforeach; ?>
</table>