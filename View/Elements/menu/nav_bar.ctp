<div class="large-2 columns" id="nav-bar">
    <ul class="side-nav">
        <li class="name"><h3><?php echo __("Menú"); ?></h3></li>
        <li class="divider"></li>
        <li id="manage-events">
        	<?php echo $this->Html->link(__('Eventos'), array('controller' => 'events', 'action' => 'index', 'admin' => true),	array('escape' => false)); ?>
        </li>
        <li class="divider"></li>
        <li id="manage-students">
        	<?php echo $this->Html->link(__('Estudiantes'), array('controller' => 'students', 'action' => 'index', 'admin' => true), array('escape' => false)); ?>
        </li>
        <li class="divider"></li>
        <li id="manage-places">
        	<?php echo $this->Html->link(__('Lugares'), array('controller' => 'places', 'action' => 'index', 'admin' => true), array('escape' => false)); ?>
        </li>
        <li class="divider"></li>
    </ul>
</div>

<script type="text/javascript">
  	$('.side-nav li a').on('click', function() {
      	$(this).parent().parent().find('.active').removeClass('active');
    	$(this).parent().addClass('active');
  	});

    // Keeps the side bar current controller active
  	$(document).ready(function () {
        var fullPath     = this.location.pathname;
        var urlPathArray = fullPath.split('/');
        var pathController = '/' + urlPathArray[1] + '/' + urlPathArray[2] + '/' + urlPathArray[3];

        $('a[href="' + pathController + '"]').parent().addClass('active');
        //$('a[href="' + this.location.pathname + '"]').parent().addClass('active');
    });
</script>