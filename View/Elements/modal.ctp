<!-- Large Modal -->
<div id="myModal-Large" class="reveal-modal large">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

<!-- Small Modal -->
<div id="myModal" class="reveal-modal small">
  <h2>Error</h2>
  <p class="lead"></p>
  <p>Sorry, but there was an error</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

