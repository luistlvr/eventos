<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	// Event states
	public $inactive_event   = 0;
	public $active_event     = 1;  
	public $list_taken_event = 2; 
	public $all_event        = 3; 

	// Students state
	public $banned_student = 0;
	public $normal_student = 1;  
	public $all_student    = 2;

	// Inscriptions state
	public $unconfirmed_inscription = 0;
	public $confirmed_inscription   = 1;
	public $absent_inscription      = 2;

	// Normal event or with punishment states
	public $normal_event     = 0;
	public $punishment_event = 1;

	// URL of the confirmation message
	public $url = "http://localhost";
	public $email = '@ucsp.edu.pe';

	// To only allow emails from the UCSP
	public $only_ucsp_email = false;

	// Componentes loaded
	public $components = array(
			'Session',
			'Auth' => array(
					'authenticate' => array(
							'Form' => array(
									'userModel' => 'User',
									'fields' => array(
											'username' => 'username',
											'password' => 'password'
									)
							)
					),
					'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),
					'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
					'authorize' => array('Controller')
			)
	);
	
	// Verify if the user is authorized for certain pages
	public function isAuthorized($user) {
	    // Admin can access every action
	    if(isset($user['role']) && $user['role'] === 'admin') {
	        return true;
	    }
    	// Default deny
    	return false;
	}


	// Load some global variables
	public function beforeFilter() {
	    if((isset($this->params['prefix']) && ($this->params['prefix'] == 'admin'))) {
	        $this->layout = 'admin';
	    }

	    $this->set('logged_in', $this->Auth->loggedIn());
		$this->set('current_user', $this->Auth->user());
	}
}
