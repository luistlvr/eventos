<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends AppController {
	public $components = array('RequestHandler', 'Email', 'Session');
	public $helpers = array('GoogleChart');

	// Global variables
	public $type_options;
	public $semester_options;
	public $credit_options;

	public function beforeFilter() {
        parent::beforeFilter();

        // Message for not authorized users
        $this->Auth->authError = "No está autorizado para acceder.";

        // Functions allowed for all users
        $this->Auth->allow('index', 'view', 'subscribe');

        // Type of event array
		$this->type_options = array(
			'chat' => 'Charla',
			'conference' => 'Conferencia',
			'meeting' => 'Conversatorio',
			'workshop' => 'Taller',
			'technical_visit' => 'Visita Técnica',
		);

		// Semesters array
		$this->semester_options = array(
			'1'  => 'I',
			'2'  => 'II',
			'3'  => 'III',
			'4'  => 'IV',
			'5'  => 'V',
			'6'  => 'VI',
			'7'  => 'VII',
			'8'  => 'VIII',
			'9'  => 'IX',
			'10' => 'X',
		);

		// Credits array
		$this->credit_options = array(
			'0'   => '0',
			'0.5' => '0.5',
			'1'   => '1',
			'1.5' => '1.5',
			'2'   => '2',
		);
    }

	/**
	 * index 
	 * Shows the events with filters
	 */
	public function index() {
		// Load the events for the search bar as autocomplete (AJAX)
		if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;

   			// Get all kind of events (with punishment or without)
   			if($_GET['punishment'] == $this->all_event) {
   				// Select the events that are not deleted
				$events = $this->Event->find('all', array(
					'limit' => 5,
					'fields' => array('DISTINCT Event.title'),
					'conditions' => array(
								'Event.title LIKE' => '%'.$_GET['term'].'%', 
								'Event.datetime >' => date('Y-m-d H:i:s'), 
								'Event.state' => $this->active_event,
								'Event.deleted' => 0))
				);
			}
			// Get events with punishment state as: $_GET['punishment'] 
			else {
				$events = $this->Event->find('all', array(
				'limit' => 5,
				'fields' => array('DISTINCT Event.title'),
				'conditions' => array(
							'Event.title LIKE' => '%'.$_GET['term'].'%', 
							'Event.datetime >' => date('Y-m-d H:i:s'), 
							'Event.state' => $this->active_event,
							'Event.punishment' => $_GET['punishment'],
							'Event.deleted' => 0))
				);
			}

			// Send the events as a json
			$i = 0;
			foreach($events as $event) {
				$response[$i]['value']   = $event['Event']['title'];
				$response[$i]['label']   = $event['Event']['title'];
				$i++;
			}
			echo json_encode($response);
		} 
		// Load the events on the page (GET)
		elseif(!empty($this->request->query)) {
			if($this->request->query['change_flag'] == 1) {
				$this->request->params['named']['page'] = 1;
				$this->request->query['change_flag'] = 0;
			}

			$this->Event->recursive = 0;

			// Get all kind of events (with punishment or without)
			if($this->request->query['punishment'] == $this->all_event) {
				$this->paginate = array(
					'limit' => 30,
					'conditions' => array(
						'Event.title LIKE' => '%' . $this->request->query['title_search'] . '%',
						'datetime >' => date('Y-m-d H:i:s'), 
						'Event.state' => $this->active_event,
						'Event.deleted' => 0
					),
				    'order' => array('datetime' => 'desc'),
				);
			} 
			// POST: Get events with punishment state as: $this->request->data['Event']['punishment']
			// GET:  Get events with punishment state as: $this->request->query['punishment']
			else {
				$this->paginate = array(
					'limit' => 30,
					'conditions' => array(
						'Event.title LIKE' => '%'.$this->request->query['title_search'].'%',
						'datetime >' => date('Y-m-d H:i:s'), 
						'Event.state' => $this->active_event,
						'Event.punishment' => $this->request->query['punishment'],
						'Event.deleted' => 0
					),
				    'order' => array('datetime' => 'desc'),
				);
			}
			
			$events = $this->paginate('Event');
			$this->set(compact('events'));

			$this->request->data['Event'] = $this->request->query;
		} 
		// All other cases
		else {
			$this->Event->recursive = 0;
			$this->paginate = array(
					'limit' => 30,
					'conditions' => array(
						'datetime >' => date('Y-m-d H:i:s'), 
						'Event.state' => $this->active_event,
						'Event.deleted' => 0),
				    'order' => array('datetime' => 'asc'),
			);
			$events = $this->paginate('Event');
			$this->set(compact('events'));
		}
	}

	/**
	 * view method
	 * Gets the information of the event
	 *
	 * @throws NotFoundException
	 * @param string $id The id of the event
	 */
	public function view($id = null) {
		// Check if te event exists
		if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido', 'flash_error'));
		}
		
		// Find the event
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
		
		// Type of event array
		$type_options = $this->type_options;

		// Send the information of the event to the view
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('type_options', 'isAjax'));
	}

	/**
	 * subscribe method
	 * Subscribe a student to an event
	 *
	 * @throws NotFoundException
	 * @param string $id
	 */
	public function subscribe($id = null) {
		// Check if the event exists
		$this->Event->id = $id;
		if(!$this->Event->exists()) {
			throw new NotFoundException('Evento inválido', 'flash_error');
		}
		
		// Verify if the request is post
		if($this->request->is('post')) {
			// Get the information of the event
			$event = $this->Event->read();

			// Verified that there is not an old event
			if($event['Event']['datetime'] < date('Y-m-d H:i:s')) {
				$event['Event']['state'] = $this->inactive_event;
				$this->Event->save($event['Event'], true, array('state'));
				
				// If its old, then redirect to the index page
				$this->Session->setFlash('El evento ya pasó!', 'flash_error');
				$this->redirect(array('action' => 'index'));
			}

			// if only ucsp mail is true, then add the @ucsp.edu.pe to the email
			if($this->only_ucsp_email) {
				$this->request->data['Student']['email'] = $this->request->data['Student']['mail'] . $this->email;
			}

			// Look for the student in the database
			$student = $this->Event->Student->find('first',  array(
				'conditions' => array('Student.email' => $this->request->data['Student']['email'], 'Student.deleted' => 0))
			);
			
        	// If its a new student, then create it
			if(empty($student)) {
				$this->Event->Student->create();
				if($this->Event->Student->save($this->request->data)) {
					$student_id = $this->Event->Student->id;
				} else {
					// If an error ocurred, then send the message with the errors
					$message = reset($this->Event->Student->validationErrors);
					$this->Session->setFlash($message['0'], 'flash_error');
					$this->redirect(array('action' => 'index'));
				}
			} 
			else {
				// If the student already is on the database, then update the semester
        		$student_id = $student['Student']['id'];
        		$student['Student']['semester'] = $this->request->data['Student']['semester'];
				$this->Event->Student->save($student, true, array('semester'));

				// Verify if the user is banned
				if($student['Student']['state'] == $this->banned_student && $student['Student']['punishment_date'] > date('Y-m-d H:i:s')) {
					$this->Session->setFlash(__('Disculpe, usted está baneado hasta el ') . $student['Student']['punishment_date'], 'flash_error');
					$this->redirect(array('action' => 'index'));
				} 
				// Change the banned state if the the date already passed
				elseif($student['Student']['state'] == $this->banned_student) {
					$student['Student']['state'] = $this->normal_student;
					$this->Event->Student->save($student, true, array('state'));
				}
        	}
			
			// Search for the activation code
			$activation_code = $this->Event->Inscription->find('first', array(
				'conditions' => array(
					'Inscription.event_id' => $id, 
					'Inscription.student_id' => $student_id,
					'Inscription.state' => $this->unconfirmed_inscription
				), 
				'contain' => array(),
				'fields' => array('id', 'activation_code', 'state')
			));

			// Email 
			$Email = new CakeEmail();
			$Email->config('gmail'); // Get config
			$Email->emailFormat('both'); // Format 
			$Email->to($this->data['Student']['email']); // Receiver
			$Email->subject('Confirmación para el evento.'); 

			$ms = 'Ingrese al siguiente link, para completar su inscripción: ';

			// If the code already exists on the database
			if(!empty($activation_code)) {
				$ms.= $this->url . '/eventos/inscriptions/verify/t:'.$activation_code['Inscription']['activation_code'].'/n:'.$activation_code['Inscription']['id'];
				$ms = wordwrap($ms, 70);
					
				$Email->send($ms);
				
				$this->Session->setFlash('Código enviado nuevamente.', 'flash_good');
				$this->redirect(array('action' => 'index'));
			} 
			// If its a new code
			else {
				// Generate the new code
				$hash = sha1(uniqid($id. $student_id . rand(0, 100), true));
				$data = array('event_id' => $id, 'student_id' => $student_id, 'activation_code' => $hash, 'semester' => $this->request->data['Student']['semester']);
				
				// Create the inscription and send the confirmation email to the student
				$this->Event->Inscription->create();
				if($this->Event->Inscription->save($data)) {
					$ms.= $this->url . '/eventos/inscriptions/verify/t:' . $hash . '/n:' . $this->Event->Inscription->id;
					$ms = wordwrap($ms, 70);
					
					$Email->send($ms);
					
					$this->Session->setFlash('Su inscripción fue exitosa, en breves minutos recibirá un mail a su correo para que confirme su inscripción.', 'flash_good');
					$this->redirect(array('action' => 'index'));
				} else {
					// Show a message in case of errors
					$message = reset($this->Event->Inscription->validationErrors);
					$this->Session->setFlash($message['0'], 'flash_error');
					$this->redirect(array('action' => 'index'));
				}
			}
		} 

		// Semesters array
		$semester_options = $this->semester_options;

		// Send the information to the view
		$only_ucsp_email = $this->only_ucsp_email;
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('semester_options', 'isAjax', 'only_ucsp_email'));
	}

	/**
	 * Here start the admin methods, only allowed to the administrator
	 */

	/**
	 * admin index method [Shows the events with pagination]
	 */
	public function admin_index() {
		// Type of event array
		$type_options = $this->type_options;

		// Load the events for the search bar as autocomplete (AJAX)
		if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;
   			// Get all events
   			if($_GET['state'] == $this->all_event) {
				$events = $this->Event->find('all', array(
					'fields' => array('DISTINCT Event.title'),
					'conditions' => array(
						'Event.title LIKE' => '%'.$_GET['term'].'%'
						, 'Event.deleted' => 0),
					'limit' => 5));
			} 
			// Get events with a defined state: $_GET['state'] (e.g. actives, inactives, list taken)
			else {
				$events = $this->Event->find('all', array(
					'fields' => array('DISTINCT Event.title'),
					'conditions' => array(
						'Event.title LIKE' => '%'.$_GET['term'].'%', 
						'Event.state' => $_GET['state'],
						'Event.deleted' => 0),
					'limit' => 5));
			}
			// Set the data as json
			$i = 0;
			foreach($events as $event) {
				$response[$i]['value']   = $event['Event']['title'];
				$response[$i]['label']   = $event['Event']['title'];
				$i++;
			}
			echo json_encode($response);
		} 
		// Get the data for the index (GET)
		elseif(!empty($this->request->query)) {
			if($this->request->query['change_flag'] == 1) {
				$this->request->params['named']['page'] = 1;
				$this->request->query['change_flag'] = 0;
			}
			
			$this->Event->recursive = 0;
			// Get all events
			if($this->request->query['state'] == $this->all_event) {
				$this->paginate = array(
					'limit' => 30,
					'conditions' => array('Event.title LIKE' => '%'.$this->request->query['title_search'].'%', 'Event.deleted' => 0),
					'order' => array('datetime' => 'desc'),
				);
			}
			// Get events with state: $this->request->data['Event']['state']
			else {
				$this->paginate = array(
					'limit' => 30,
					'conditions' => array(
						'Event.title LIKE' => '%'.$this->request->query['title_search'].'%', 
						'Event.state' => $this->request->query['state'],
						'Event.deleted' => 0),
					'order' => array('datetime' => 'desc'),
				);
			}
			
			// Send the data to the view
			$events = $this->paginate('Event');
			$this->set(compact('events', 'type_options'));

			$this->request->data['Event'] = $this->request->query;
		} 
		// Send the events to the view
		else {
			$this->Event->recursive = 0;
			$this->paginate = array(
					'limit' => 30,
					'conditions' => array('Event.deleted' => 0),
				    'order' => array('datetime' => 'desc'),
			);
			$events = $this->paginate('Event');
			$this->set(compact('events', 'type_options'));
		}
	}

	/**
	 * admin add method
	 * Creates a new event
	 */
	public function admin_add() {
		if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;

			$places = $this->Event->Place->find('all', array(
					'fields' => array('DISTINCT Place.name, Place.capacity'),
					'conditions' => array('Place.name LIKE' => '%'.$_GET['term'].'%', 'Place.deleted' => 0),
					'limit' => 5));
			
			$i = 0;
			foreach($places as $place) {
				$response[$i]['value']    = $place['Place']['name'];
				$response[$i]['label']    = $place['Place']['name'];
				$response[$i]['capacity'] = $place['Place']['capacity'];
				$i++;
			}
			echo json_encode($response);
		} 
		elseif($this->request->is('post')) {
			if(!empty($this->request->data['Event']['file']['name'])) {
            	$file = $this->request->data['Event']['file']; //put the data into a var for easy use
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                //only process if the extension is valid
                
                if(in_array($ext, $arr_ext)) {
                	//do the actual uploading of the file. 
                	$file['name'] = uniqid() . '.' . $ext;
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/uploads/events/' . $file['name']);

                    //prepare the filename for database entry
                    $this->request->data['Event']['image'] = $file['name'];
                }
            }

            $this->request->data['Event']['start_time'] = date('Y-m-d H:i:s', strtotime($this->request->data['Event']['start_time']));
            $this->request->data['Event']['end_time']   = date('Y-m-d H:i:s', strtotime($this->request->data['Event']['end_time']));
			
			$this->Event->create();
			if($this->Event->save($this->request->data)) {
				$this->Session->setFlash('El evento ha sido agregado', 'flash_good');
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash('El evento no pudo ser agregado', 'flash_error');
			}
		}
		
		// Type of event array
		$type_options = $this->type_options;

		// Credits array
		$credit_options = $this->credit_options;

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('type_options', 'credit_options', 'isAjax'));
	}

	/**
	 * admin view method [Shows the information of an event]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_view($id = null) {
		if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido.', 'flash_error'));
		}
		
		// Get the information of the event and send it to the view
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
		
		// Type of event array
		$type_options = $this->type_options;

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('type_options', 'isAjax'));
	}

	/**
	 * admin edit method [Edits the information of an event]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_edit($id = null) {		
		if(!$this->Event->exists($id)) {
			throw new NotFoundException('Evento inválido', 'flash_error');
		}

		if($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->request->data['Event']['file']['name'])) {
            	$file = $this->request->data['Event']['file']; //put the data into a var for easy use
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                //only process if the extension is valid
                
                if(in_array($ext, $arr_ext)) {
                	//do the actual uploading of the file. 
                	$file['name'] = uniqid() . '.' . $ext;
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/uploads/events/' . $file['name']);

                    //prepare the filename for database entry
                    $this->request->data['Event']['image'] = $file['name'];
                }
            }

            $this->request->data['Event']['start_time'] = date('Y-m-d H:i:s', strtotime($this->request->data['Event']['start_time']));
            $this->request->data['Event']['end_time']   = date('Y-m-d H:i:s', strtotime($this->request->data['Event']['end_time']));

			if($this->Event->save($this->request->data)) {
				$this->Session->setFlash('El evento ha sido salvado', 'flash_good');
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash('El evento no pudo ser salvado. Intente luego', 'flash_error');
			}
		} 

		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->request->data = $this->Event->find('first', $options);
		
		// Type of event array
		$type_options = $this->type_options;
		
		// Credits array
		$credit_options = $this->credit_options;
		
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('type_options', 'credit_options', 'isAjax'));
	}

	/**
	 * admin delete method [Deletes an event]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_delete($id = null) {
		$this->Event->id = $id;
		if(!$this->Event->exists()) {
			throw new NotFoundException('Evento inválido.', 'flash_error');
		}
		
		// Only allow the request if its POST or DELETE
		$this->request->onlyAllow('post', 'delete');

		// It's just a logical delete, not a physic
		//if($this->Event->delete()) {
		if($this->Event->saveField('deleted', 1)) {
			$this->Session->setFlash('Evento borrado', 'flash_good');
		} else {
			$this->Session->setFlash('El evento no fue borrado', 'flash_error');
		}
		
		$this->redirect(array('action' => 'index', 'admin' => true));
	}

	/**
	 * [admin_view_pdf Handles the pdf view of the event]
	 * @param  [int] $id [Id of the event]
	 */
	public function admin_view_pdf($id = null) {
	    if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido.', 'flash_error'));
		}
		
	    // increase memory limit in PHP
	    ini_set('memory_limit', '512M');
	    
	    // Get the information of the event, like title, students, etc.
	    $options = array(
			'conditions' => array(
				'Event.id' => $id
			), 
			'contain' => array(
				'Student' => array(
					'conditions' => array('Inscription.state' => '1', 'Student.deleted' => 0),
					'fields' => array('Student.id', 'Student.fullname', 'Student.email', 'Student.code'),
					'order' => 'Student.first_surname ASC',
				),
			),
			'fields' => array('Event.id', 'Event.title', 'Event.speaker', 'Event.datetime')
		);
		
		// Sends the information to the view
		$students = $this->Event->find('first', $options);
	    $this->set('students', $students['Student']);
	    $this->set('event', $students['Event']);
	}

	/**
	 * [admin_view_excel Handles the excel view of an event]
	 * @param  [int] $id [Id of the event]
	 */
	function admin_view_excel($id = null) {
		if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido', 'flash_error'));
		}
		
		// Get the information of the event, like title, students, etc.
		$this->layout='ajax';
		$options = array(
			'conditions' => array(
				'Event.id' => $id
			), 
			'contain' => array(
				'Student' => array(
					'conditions' => array('Inscription.state' => '1', 'Student.deleted' => 0),
					'fields' => array('Student.id', 'Student.fullname', 'Student.email', 'Student.code'),
					'order' => 'Student.first_surname ASC',
				),
			),
			'fields' => array('Event.id', 'Event.title', 'Event.speaker', 'Event.datetime')
		);
		
		// Sends the information to the view
		$students = $this->Event->find('first', $options);
	    $this->set('students', $students['Student']);
	    $this->set('event', $students['Event']);
	}

	/**
	 * [admin_send_mail Send a email to the students inscribed in a event]
	 * @param  [int] $id [Id of the event]
	 */
	public function admin_send_mail($id = null) {
		$this->Event->id = $id;
		if(!$this->Event->exists()) {
			throw new NotFoundException('Evento inválido', 'flash_error');
		}

		// Only if the request is POST or PUT
		if($this->request->is('post') || $this->request->is('put')) {
			$options = array(
				'conditions' => array('Event.' . $this->Event->primaryKey => $id),
				'contain' => array(
					'Student' => array(
						'conditions' => array('Inscription.state' => '1'),
						'fields' => array('Student.email'),
					)
				),
				'fields' => array('Event.id'));
			$this->Event->recursive = 1;
			$users = $this->Event->find('first', $options);

			// Add emails of the students to the array
			$bcc = array();
		    foreach($users['Student'] as $user) {
		        $bcc[] = $user['email'];
		    }
		    
		    // If there are no students, then show a message
		    if(empty($bcc)) {
		    	$this->Session->setFlash('No hay destinatarios', 'flash_error');
		    } 
		    // Send the message to the inscribed students
		    else {
		    	try {
				    $Email = new CakeEmail();
					$Email->config('gmail');
					$Email->emailFormat('both');
					if(!empty($this->request->data['Message']['attach']))
						$Email->attachments($_SERVER['DOCUMENT_ROOT'] . Router::url('/app/webroot/uploadify/files/') . $this->request->data['Message']['attach']);
					$Email->bcc($bcc);
					$Email->subject($this->request->data['Message']['title']);
					$Email->send($this->request->data['Message']['message']);

					$this->Session->setFlash('Mensaje enviado', 'flash_good');

					$file = new File($_SERVER['DOCUMENT_ROOT'] . Router::url('/app/webroot/uploadify/files/') . $this->request->data['Message']['attach']);
					if($file->exists())
						$file->delete();

					$this->redirect(array('action' => 'view', $id, 'admin' => true));
				} catch(Exception $e) {
					debug($e);
					$this->Session->setFlash('Mensaje no enviado', 'flash_error');
				}	
		    }
		} 	
		$event = $this->Event->read();
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax', 'id', 'event'));
	}

	/**
	 * [admin_take_list Take the list of the students who assist to the event]
	 * @param  [int] $id [Id of the event]
	 */
	public function admin_take_list($id = null) {
    	if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido', 'flash_error'));
		}
		
		$options = array(
			'conditions' => array(
				'Event.id' => $id
			),
			'contain' => array(
				'Student' => array(
					'conditions' => array('Inscription.state !=' => '0', 'Student.deleted' => 0),
					'fields' => array('Student.id', 'Student.fullname', 'Student.email', 'Student.code'),
					'order' => 'Student.fullname ASC',
				),
			),
			'fields' => array('Event.id, Event.punishment', 'Event.title', 'Event.place', 'Event.credits', 'Event.speaker', 'Event.date')
		);
			
		$event = $this->Event->find('first', $options); 
		$students = $event['Student'];

		if($this->request->is('post')) {
			if(isset($this->params['data']['cancel'])) {
				$this->redirect(array('action' => 'view', $id, 'admin' => true));
			}

			$inscription_group = array();
			if(empty($this->request->data['Event'])) {
				foreach($students as $student) {
					$inscription_group[] = array('id' => $student['Inscription']['id'], 'state' => $this->confirmed_inscription);
				}
				
				$this->Event->Inscription->saveAll($inscription_group);
				$this->Session->setFlash('Lista Tomada.', 'flash_good');
			} else {
				foreach($students as $student) {
					$inscription_group[] = array('id' => $student['Inscription']['id'], 'state' => $this->confirmed_inscription);
				}

				$student_group = array();
				foreach($this->request->data['Event'] as $inscription) {
					$fields_ids = split(":", $inscription);
					$inscription_group[] = array('id' => $fields_ids[0], 'state' => $this->absent_inscription);
					
					if($event['Event']['punishment'] == $this->punishment_event) {
						$effectiveDate = strtotime("+3 months", strtotime(date('Y-m-d H:i:s'))); 
						$student_group[] = array('id' => $fields_ids[1], 'state' => $this->banned_student, 'punishment_date' => date('Y-m-d H:i:s', $effectiveDate));
					}
				}

				$this->Event->Inscription->saveAll($inscription_group);
				
				if(!empty($student_group))
					$this->Event->Student->saveAll($student_group);
			}

			$this->Event->read(null, $id);
			$this->Event->set('state', $this->list_taken_event);
			if($this->Event->save()) {
				$this->Session->setFlash('Lista Tomada.', 'flash_good');
			} else {
				$this->Session->setFlash('Ocurrio un error.', 'flash_error');
			}

			$this->redirect(array('action' => 'index', 'admin' => true));
		} 

		$absent = $this->absent_inscription;
		$isAjax = $this->RequestHandler->isAjax();
    	$this->set(compact('students', 'isAjax', 'absent', 'event'));
    }

    /**
	 * admin add student method
	 *
	 * @throws NotFoundException
	 * @param int $id
	 * @return void
	 */
	public function admin_add_student($id = null) {
		$this->Event->id = $id;
		if(!$this->Event->exists()) {
			throw new NotFoundException('Evento Inválido', 'flash_error');
		}
		
		if($this->request->is('post')) {
			// Update the email adding @ucsp.edu.pe at the end
			if($this->only_ucsp_email) {
				$this->request->data['Student']['email'] = $this->request->data['Student']['mail'] . $this->email;
			}

			// Look for the student
			$student = $this->Event->Student->find('first',  array(
				'conditions' => array('Student.email' => $this->request->data['Student']['email'], 'Student.deleted' => 0))
			);
			
        	// if its a new student, then create it
			if(empty($student)) {
				$this->Event->Student->create();
				if($this->Event->Student->save($this->request->data)) {
					$student_id = $this->Event->Student->id;
				} /*else {
					// Se envía un mensaje por los errores de validación
					$message = reset($this->Event->Student->validationErrors);
					$this->Session->setFlash($message['0'], 'flash_error');
					$this->redirect(array('action' => 'admin_take_list'));
				}*/
			} 
			else {
				// if its not a new student, then update the semester
        		$student_id = $student['Student']['id'];
        		$student['Student']['semester'] = $this->request->data['Student']['semester'];
				$this->Event->Student->save($student, true, array('semester'));

				// Change the state of the student in case its not banned anymore
				if($student['Student']['state'] == $this->banned_student) {
					$student['Student']['state'] = $this->normal_student;
					$this->Event->Student->save($student, true, array('state'));
				}
        	}

        	if(isset($student_id)) {
        		$inscription = $this->Event->Inscription->find('first', array(
					'conditions' => array(
						'Inscription.event_id' => $id, 
						'Inscription.student_id' => $student_id,
					), 
					'fields' => array('id', 'state')
				));
        		
	        	if(empty($inscription)) {
	        		$data = array('event_id' => $id, 'student_id' => $student_id, 'semester' => $this->request->data['Student']['semester'], 'state' => $this->confirmed_inscription);
					
					// Create the inscription
					$this->Event->Inscription->create();
					if($this->Event->Inscription->save($data)){
						$this->Session->setFlash(__('Estudiante agregado'), 'flash_good');
						$this->redirect(array('action' => 'take_list', $id, 'admin' => true));
					} else {
						// Send a message in case of error
						//$message = reset($this->Event->Inscription->validationErrors);
						//$this->Session->setFlash($message['0'], 'flash_error');
						$this->Session->setFlash('El Estudiante no se pudo inscribir', 'flash_error');
					}
				}
				elseif($inscription['Inscription']['state'] == $this->unconfirmed_inscription) {
					$inscription['Inscription']['state'] = $this->confirmed_inscription;
					$this->Event->Inscription->save($inscription);

					$this->Session->setFlash(__('Estudiante inscrito.'), 'flash_good');
					$this->redirect(array('action' => 'take_list', $id, 'admin' => true));
	        	} else {
	        		$this->Session->setFlash(__('Estudiante ya inscrito.'), 'flash_error');
	        	}
        	}
		} 

		// Semesters array
		$semester_options = $this->semester_options;

		$only_ucsp_email = $this->only_ucsp_email;
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('semester_options', 'isAjax', 'id', 'only_ucsp_email'));
	}

	/**
	 * admin view_chart method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 */
	public function admin_view_chart($id = null) {
		if(!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento inválido.', 'flash_error'));
		}
		
		$labels = array('primer', 'segundo', 'tercer', 'cuarto', 'quinto', 'sexto', 'séptimo', 'octavo', 'noveno', 'décimo');
		$semester_labels = array();
		$semester_values = array();
		for($i = 1; $i <= 10; $i++) {
			$options = array(
				'conditions' => array(
					'Inscription.event_id' => $id,
					'Inscription.state !=' => '0',
					'Inscription.semester' => $i,
				), 
			);
			$semester = $this->Event->Inscription->find('count', $options);
			if($semester > 0) {
				$semester_values[] = $this->Event->Inscription->find('count', $options);
				$semester_labels[] = $labels[$i];
			}
				
		}

		$options = array(
			'conditions' => array(
				'Event.id' => $id
			), 
			'fields' => array('Event.title')
		);
			
		$event = $this->Event->find('first', $options); 
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax', 'semester_values', 'event', 'semester_labels'));
	}
}