<?php
App::uses('AppController', 'Controller');
/**
 * Places Controller
 *
 * @property Event $Event
 */
class PlacesController extends AppController {
	public $components = array('RequestHandler', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authError = "No está autorizado para acceder.";
    }

    /**
	 * admin index method [Show the places]
	 *
	 */
	public function admin_index() {
		// If is AJAX, used for the autocomplete on the search bar
		if($this->RequestHandler->isAjax()) { 
   			$this->autoRender = false;

			$places = $this->Place->find('all', array(
					'fields' => array('DISTINCT Place.name'),
					'conditions' => array('Place.name LIKE' => '%'.$_GET['term'].'%', 'Place.deleted' => 0),
					'limit' => 5));
			$i = 0;
			foreach($places as $place) {
				$response[$i]['value']   = $place['Place']['name'];
				$response[$i]['label']   = $place['Place']['name'];
				$i++;
			}
			echo json_encode($response);
		} 
		// If its a GET search
		elseif(!empty($this->request->query)) {
			if($this->request->query['change_flag'] == 1) {
				$this->request->params['named']['page'] = 1;
				$this->request->query['change_flag'] = 0;
			}

			$this->Place->recursive = 0;
			$this->paginate = array(
					'limit' => 30,
					'conditions' => array('Place.name LIKE' => '%'.$this->request->query['title_search'].'%', 'Place.deleted' => 0),
					'order' => array('name' => 'desc'),
			);

			$places = $this->paginate('Place');
			$this->set(compact('places'));

			$this->request->data['Place'] = $this->request->query;
		} 
		// Show the places
		else {
			$this->Place->recursive = 0;
			$this->paginate = array(
					'limit' => 30,
					'conditions' => array('Place.deleted' => 0),
				    'order' => array('name' => 'desc'),
			);
			$places = $this->paginate('Place');
			$this->set(compact('places'));
		}
	}

	/**
	 * admin add method [Add a place to the database]
	 *
	 */
	public function admin_add() {
		// Only allow post
		if($this->request->is('post')) {			
			$this->Place->create();
			if($this->Place->save($this->request->data)) {
				$this->Session->setFlash('El lugar ha sido agregado', 'flash_good');
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash('El lugar no pudo ser agregado', 'flash_error');
			}
		}

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax'));
	}

	/**
	 * admin view method [Show the information of the place]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_view($id = null) {
		// Look if the place exists on the database
		if(!$this->Place->exists($id)) {
			throw new NotFoundException(__('Lugar inválido', 'flash_error'));
		}
		
		$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id));
		$this->set('place', $this->Place->find('first', $options));

		// Sends the data to the view
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax'));
	}

	/**
	 * admin edit method [Edit the information of a place]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_edit($id = null) {		
		// Look if the place exists on the database
		if(!$this->Place->exists($id)) {
			throw new NotFoundException('Lugar inválido', 'flash_error');
		}

		// Only allow POST and PUT
		if($this->request->is('post') || $this->request->is('put')) {
			if($this->Place->save($this->request->data)) {
				$this->Session->setFlash('El lugar ha sido salvado', 'flash_good');
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash('El lugar no pudo ser salvado. Intente luego', 'flash_error');
			}
		} 

		$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id));
		$this->request->data = $this->Place->find('first', $options);
		
		// Sends the data to the view
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax'));
	}

	/**
	 * admin delete method [Deletes a place]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_delete($id = null) {
		$this->Place->id = $id;
		if(!$this->Place->exists()) {
			throw new NotFoundException('Lugar inválido.', 'flash_error');
		}
		
		// Only allow delete with POST and DELETE
		$this->request->onlyAllow('post', 'delete');
		//if($this->Place->delete()) {
		// Just logical delete
		if($this->Place->saveField('deleted', 1)) {
			$this->Session->setFlash('Lugar borrado.', 'flash_good');
		} else {
			$this->Session->setFlash('El lugar no fue borrado.', 'flash_error');
		}
		
		$this->redirect(array('action' => 'index', 'admin' => true));
	}
}