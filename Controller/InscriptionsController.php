<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Inscription Controller
 *
 * @property Inscription $Inscription
 */
class InscriptionsController extends AppController {
	public $components = array('RequestHandler', 'Email', 'Session');
	public $helpers = array('Js', 'Html', 'Form');
	
	public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authError = "No está autorizado para acceder.";
        $this->Auth->allow('verify');
    }

    /**
     * [verify Verifies the correct inscription of a student]
     * @return [type] [description]
     */
	public function verify() {
		$dias   = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
		$meses  = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$punish = array("No", "Si");
		$type_options = array(
			'chat' => 'Charla',
			'conference' => 'Conferencia',
			'meeting' => 'Conversatorio',
			'workshop' => 'Taller',
			'technical_visit' => 'Visita Técnica',
		);

		// Get data from url			
		if(!empty($this->passedArgs['n']) && !empty($this->passedArgs['t'])) {
			$id = $this->passedArgs['n'];
			$activation_code = $this->passedArgs['t'];
			
			$this->Inscription->recursive = 1; 
			$this->Inscription->id = $id;
			$result = $this->Inscription->read();

			// Verify if is an old event, or it reachead a limit
			if($result['Event']['state'] != $this->active_event) {
				$this->Session->setFlash('Este evento ya no esta activo!', 'flash_error');
				$this->redirect(array('controller' => 'events', 'action' => 'index'));
			}
			elseif($result['Event']['datetime'] < date('Y-m-d H:i:s')) {
				$result['Event']['state'] = $this->inactive_event;
				$this->Inscription->Event->save($result['Event'], true, array('state'));
				
				$this->Session->setFlash('El evento ya pasó!', 'flash_error');
				$this->redirect(array('controller' => 'events', 'action' => 'index'));
			}
			// If the inscription is not yet confirmed
			elseif($result['Inscription']['state'] == $this->unconfirmed_inscription) {
				if($result['Inscription']['activation_code'] == $activation_code) {
					$message = "";
					// Creates an email with the information of the event
					if(!is_null($result['Event']['speaker'])) {
						$message .= "<p><strong>Ponenete: </strong>" . $result['Event']['speaker'] . "</p>";
					}

					if(!is_null($result['Event']['title'])) {
						$message .= "<p><strong>Descripción: </strong>" . $result['Event']['title'] . "</p>";
					}
					// Description of the event
					$message .= "<p><b>" . __('Lugar: ') . "</b>" . $result['Event']['place'] . "</b></p>";
					$message .= "<p><b>" . __('Tipo: ') . "</b>" . $type_options[$result['Event']['type']] . "</b></p>";
					$message .= "<p><b>" . __('Crédito(s): ') . "</b>" . $result['Event']['credits'] . "</b></p>";
					$message .= "<p><b>" . __('Capacidad: ') . "</b>" . $result['Event']['capacity'] . "</b></p>";
					$message .= "<p><b>" . __('Penalidad: ') . "</b>" . $punish[$result['Event']['punishment']] . "</b></p>";

					$message .= "<p><b>" . __('Fecha: ') . $dias[date('w', strtotime($result['Event']['date']))].", ".date('d', strtotime($result['Event']['date']))." de ".$meses[date('n', strtotime($result['Event']['date']))-1]. " del " . date('Y', strtotime($result['Event']['date'])) . "</b></p>";
					$message .= "<p><b>" . __('Hora de Inicio: ') . date('h:i A', strtotime($result['Event']['start_time'])) . "</b></p>";
					$message .= "<p><b>" . __('Hora de Fin: ')    . date('h:i A', strtotime($result['Event']['end_time'])) . "</b></p>";
					
					//Set state of the inscription to confirmed
					$result['Inscription']['state'] = $this->confirmed_inscription;
					
					//Add one student to the event
					$result['Event']['inscriptions_count'] += 1;
		
					//Save the data
					$this->Inscription->save($result['Inscription'], true, array('state'));
					$this->Inscription->Event->save($result['Event'], true, array('state', 'inscriptions_count'));

					//Send email with event info
					$Email = new CakeEmail();
					$Email->config('gmail');
					$Email->emailFormat('both');
					$Email->to($result['Student']['email']);
					$Email->subject($result['Event']['title']);
					$Email->send($message);
					
					$this->Session->setFlash('Tu inscripción está completa!', 'flash_good');
					$this->redirect(array('controller' => 'events', 'action' => 'index'));
				} else{
					$this->Session->setFlash('Tu inscripción falló, por favor intenta de nuevo', 'flash_error');
					$this->redirect(array('controller' => 'events', 'action' => 'index'));
				}
			} else {
				$this->Session->setFlash('Usted ya está inscrito', 'flash_error');
				$this->redirect(array('controller' => 'events', 'action' => 'index'));
			}
		} else {
			$this->Session->setFlash('Token corrupto', 'flash_error');
			$this->redirect(array('controller' => 'events', 'action' => 'index'));
		}
	}
}
