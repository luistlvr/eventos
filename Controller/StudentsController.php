<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Students Controller
 *
 * @property Student $Student
 */
class StudentsController extends AppController {
	public $components = array('RequestHandler', 'Session', 'Email');

	// Global variables
	public $state_options;
	public $semester_options;

	public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authError = "No está autorizado para acceder.";

        // States array
        $this->state_options = array(
			'0' => 'Penalizado',
			'1' => 'Normal'
		);

		// Semesters array
		$this->semester_options = array(
			'1'  => 'I',
			'2'  => 'II',
			'3'  => 'III',
			'4'  => 'IV',
			'5'  => 'V',
			'6'  => 'VI',
			'7'  => 'VII',
			'8'  => 'VIII',
			'9'  => 'IX',
			'10' => 'X',
		);
    }
	
	/**
	 * [admin_index Wait for a search to list the students]
	 */
	public function admin_index() {
		if(!empty($this->request->query)) {
			// If its a change, then return to the first page
			if($this->request->query['change_flag'] == 1) {
				$this->request->params['named']['page'] = 1;
				$this->request->query['change_flag'] = 0;
			}

			$options = array();

			// Avoid the logic deleted students
		 	$options += array('Student.deleted' => 0);
			
			// Gets the first surname for the search if exists
			if(!empty($this->request->query['first_surname'])) {
				$firstsurname = array("Student.first_surname LIKE" => $this->request->query['first_surname'].'%');
				$options += $firstsurname;
			}
			
			// Gets the second surname for the search if exists
			if(!empty($this->request->query['second_surname'])) {
				$secondsurname = array("Student.second_surname LIKE" => $this->request->query['second_surname'].'%');
				$options += $secondsurname;
			}
			
			// Gets the names for the search if exists
			if(!empty($this->request->query['names'])) {
				$names = array("Student.names LIKE" => $this->request->query['names'].'%');
				$options += $names;
			}
			
			// Gets the state of the student for the search
			if($this->request->query['state'] != $this->all_student)
				$options += array("Student.state" => $this->request->query['state']);

			$this->paginate = array(
				'conditions' => $options,
		    	'order' => array('Student.first_surname' => 'asc'),
			);
			$students = $this->paginate('Student');

			$this->request->data['Student'] = $this->request->query;
		}
		$this->set(compact('students'));
    }

    /**
     * [admin_search_name Search the names on the database for the autocomplete]
     */
    public function admin_search_name() {
    	if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;
   			// If the state its all students
   			if($_GET['state'] == $this->all_student) {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.names'),
					'conditions' => array('Student.names LIKE' => '%'.$_GET['term'].'%', 'Student.deleted' => 0),
					'limit' => 5)
				);
			}
			// Any other state 
			else {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.names'),
					'conditions' => array(
						'Student.names LIKE' => '%'.$_GET['term'].'%', 
						'Student.state' => $_GET['state'], 
						'Student.deleted' => 0),
					'limit' => 5
					)
				);
			}
			$i = 0;
			foreach($students as $student) {
				$response[$i]['value']   = $student['Student']['names'];
				$response[$i]['label']   = $student['Student']['names'];
				$i++;
			}
			echo json_encode($response);
		}
    }

    /**
     * [admin_search_first_surname Search the first surename on the database for the autocomplete]
     */
    public function admin_search_first_surname() {
    	if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;
   			// If the state its all students
   			if($_GET['state'] == $this->all_student) {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.first_surname'),
					'conditions' => array('Student.first_surname LIKE' => '%'.$_GET['term'].'%', 'Student.deleted' => 0),
					'limit' => 5)
				);
			}
			// Any other state  
			else {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.first_surname'),
					'conditions' => array(
						'Student.first_surname LIKE' => '%'.$_GET['term'].'%', 
						'Student.state' => $_GET['state'], 
						'Student.deleted' => 0),
					'limit' => 5
					)
				);
			}
			$i = 0;
			foreach($students as $student) {
				$response[$i]['value']   = $student['Student']['first_surname'];
				$response[$i]['label']   = $student['Student']['first_surname'];
				$i++;
			}
			echo json_encode($response);
		}
    }

    /**
     * [admin_search_second_surname Search the second surename on the database for the autocomplete]
     */
    public function admin_search_second_surname() {
    	if($this->RequestHandler->isAjax()) {
   			$this->autoRender = false;
   			// If the state its all students
   			if($_GET['state'] == $this->all_student) {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.second_surname'),
					'conditions' => array('Student.second_surname LIKE' => '%'.$_GET['term'].'%', 'Student.deleted' => 0),
					'limit' => 5)
				);
			}
			// Any other state  
			else {
				$students = $this->Student->find('all', array(
					'fields' => array('DISTINCT Student.second_surname'),
					'conditions' => array(
						'Student.second_surname LIKE' => '%'.$_GET['term'].'%', 
						'Student.state' => $_GET['state'], 
						'Student.deleted' => 0),
					'limit' => 5
					)
				);
			}
			$i = 0;
			foreach($students as $student) {
				$response[$i]['value']   = $student['Student']['second_surname'];
				$response[$i]['label']   = $student['Student']['second_surname'];
				$i++;
			}
			echo json_encode($response);
		}
    }

	/**
	 * admin add method [Add a new student]
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_add() {		
		if($this->request->is('post') || $this->request->is('put')) {
			// Update the email adding @ucsp.edu.pe at the end
			if($this->only_ucsp_email) {
				$this->request->data['Student']['email'] = $this->request->data['Student']['mail'] . $this->email;
			}

			$conditions = array(
			    'Student.email' => $this->request->data['Student']['email'],
			    'Student.deleted' => 0
			);

			// Look for the student in the database
			if($this->Student->hasAny($conditions)) {
				$student['Student']['semester'] = $this->request->data['Student']['semester'];
				$this->Student->save($student, true, array('semester'));
			    $this->Session->setFlash('El estudiante ya existe', 'flash_good');
				$this->redirect(array('action' => 'index'));
			}
			// Save the new student
			elseif($this->Student->save($this->request->data)) {
				$this->Session->setFlash('El estudiante ha sido salvado', 'flash_good');
				$this->redirect(array('action' => 'index'));
			}
			// Error 
			else {
				$this->Session->setFlash('El estudiante no pudo ser salvado', 'flash_error');
			}
		} 

		// Student states array
		$state_options = $this->state_options;

		// Semesters array
		$semester_options = $this->semester_options;

		// Allow only ucsp email
		$only_ucsp_email = $this->only_ucsp_email;

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('semester_options', 'state_options', 'isAjax', 'only_ucsp_email'));	
	}
    
	/**
	 * admin view method [View the data of an specific student]
	 *
	 * @throws NotFoundException
	 * @param int $id [Id of the student]
	 */
	public function admin_view($id = null) {
		// Look for the student on the database
		if(!$this->Student->exists($id)) {
			throw new NotFoundException(__('Estudiante inválido'));
		}

		$options = array(
			'conditions' => array(
				'Student.' . $this->Student->primaryKey => $id),
			'contain' => array(
				'Event' => array(
					'conditions' => array(
						'Inscription.state' => '1',
						'Event.state' => '2',
					),
				)
			)
		);
		
		// Get the student 
		$student = $this->Student->find('first', $options);
		$states = array(
			'0' => 'Penalizado',
			'1' => 'Normal'
		);

		// Send the student data to the view
		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('states', 'student', 'isAjax'));
	}
	
	/**
	 * admin edit method [Edit the information of a specific student]
	 *
	 * @throws NotFoundException
	 * @param int $id [Id of the student]
	 */
	public function admin_edit($id = null) {	
		// Look for the student on the database	
		if(!$this->Student->exists($id)) {
			throw new NotFoundException(__('Estudiante inválido'));
		}

		// Only allow POST and PUT 
		if($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Student']['email'] = $this->request->data['Student']['mail'] . $this->email;
			$this->request->data['Student']['punishment_date'] = $this->request->data['Student']['punishment_date'] . ' ' . $this->request->data['Student']['punishment_time'];
			if($this->Student->save($this->request->data)) {
				$this->Session->setFlash('El estudiante ha sido salvado.', 'flash_good');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('El estudiante no pudo ser salvado.', 'flash_error');
			}
		} 

		$options = array('conditions' => array('Student.' . $this->Student->primaryKey => $id));
		$this->request->data = $this->Student->find('first', $options);
		$this->request->data['Student']['punishment_time'] = date('H:i:s', strtotime($this->request->data['Student']['punishment_date']));
		$this->request->data['Student']['punishment_date'] = date('Y-m-d', strtotime($this->request->data['Student']['punishment_date']));
		
		$str = $this->request->data['Student']['email'];
		$mail = explode("@",$str);
		array_pop($mail);
		$mail = implode("@",$mail);

		$this->request->data['Student']['mail'] = $mail;

		// Student states array
		$state_options = $this->state_options;

		// Semesters array
		$semester_options = $this->semester_options;

		// Allow only ucsp email
		$only_ucsp_email = $this->only_ucsp_email;

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('semester_options', 'state_options', 'isAjax', 'only_ucsp_email'));		
	}

	/**
	 * [admin_view_pdf Generates a pdf of the student's information]
	 * @param  [int] $id [Id of the student]
	 */
	public function admin_view_pdf($id = null) {
	    if(!$this->Student->exists($id)) {
			throw new NotFoundException(__('Estudiante inválido.', 'flash_error'));
		}
		
	    // increase memory limit in PHP
	    ini_set('memory_limit', '512M');
	    
	    // Get the student's information
	    $options = array(
			'conditions' => array(
				'Student.' . $this->Student->primaryKey => $id),
			'contain' => array(
				'Event' => array(
					'conditions' => array(
						'Inscription.state' => '1',
						'Event.state' => '2',
					),
				)
			)
		);
		
		// Send the information to the view
		$student = $this->Student->find('first', $options);
		$this->set(compact('student'));
	}

	/**
	 * [admin_send_mail Send a email to a student]
	 * @param  [int] $id [Id of the student]
	 */
	public function admin_send_mail($id = null) {
		$this->Student->id = $id;
		if(!$this->Student->exists()) {
			throw new NotFoundException('Estudiante inválido', 'flash_error');
		}
		$student = $this->Student->read();

		if($this->request->is('post') || $this->request->is('put')) {
			try {
				// Create the email
			    $Email = new CakeEmail();
				$Email->config('gmail');
				$Email->emailFormat('both');

				// Attach file, in case exists
				if(!empty($this->request->data['Message']['attach']))
					$Email->attachments($_SERVER['DOCUMENT_ROOT'] . Router::url('/app/webroot/uploadify/files/') . $this->request->data['Message']['attach']);

				// Send the email
				$Email->to($student['Student']['email']);
				$Email->subject($this->request->data['Message']['title']);
				$Email->send($this->request->data['Message']['message']);

				$this->Session->setFlash('Mensaje enviado', 'flash_good');

				$file = new File($_SERVER['DOCUMENT_ROOT'] . Router::url('/app/webroot/uploadify/files/') . $this->request->data['Message']['attach']);
				if($file->exists())
					$file->delete();

				$this->redirect(array('action' => 'view', $id, 'admin' => true));
			} catch(Exception $e) {
				debug($e);
				$this->Session->setFlash('Mensaje no enviado', 'flash_error');
			}
		} 	

		$isAjax = $this->RequestHandler->isAjax();
		$this->set(compact('isAjax', 'id', 'student'));
	}
	
	/**
	 * admin delete method
	 *
	 * @throws NotFoundException
	 * @param int $id
	 */
	public function admin_delete($id = null) {
		$this->Student->id = $id;
		if(!$this->Student->exists()) {
			throw new NotFoundException(__('Estudiante inválido.'));
		}
		
		// Only allow POST or DELETE
		$this->request->onlyAllow('post', 'delete');

		// Only logical delete of the student
		//if($this->Student->delete()) {
		if($this->Student->saveField('deleted', 1)) {
			$this->Session->setFlash('Estudiante borrado', 'flash_good');
		} else {
			$this->Session->setFlash('El estudiante no fue borrado', 'flash_error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
