<?php
class UsersController extends AppController {
	public $components = array('Session', 'RequestHandler');
	
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authError = "No está autorizado para acceder.";
    }

    /**
     * [admin_index Show the current users]
     */
    public function admin_index() {
        // If its AJAX, means that is an autocomplete query for the search bar
        if($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $users = $this->User->find('all', array(
                'fields' => array('DISTINCT User.username'),
                'conditions' => array('User.username LIKE' => '%'.$_GET['term'].'%', 'User.deleted' => 0)));
            $i = 0;
            foreach($users as $user) {
                $response[$i]['value']   = $user['User']['username'];
                $response[$i]['label']   = $user['User']['username'];
                $i++;
            }
            echo json_encode($response);
        } 
        // If its a post then is a search
        elseif($this->request->is('post')) {
            $this->paginate = array(
                    'limit' => 30,
                    'conditions' => array(
                        'User.username LIKE' => '%'.$this->request->data['User']['username_search'].'%',
                        'User.deleted' => 0
                    ),
            );
            $users = $this->paginate('User');
        } 
        else {
            $this->paginate = array(
                    'limit' => 30,
                    'conditions' => array(
                        'User.deleted' => 0
                    ),
            );
            $users = $this->paginate('User');
        }

        $this->set(compact('users'));
    }

    /**
     * [admin_dashboard main window]
     */
    public function admin_dashboard() {
        
    }

    /**
     * [admin_login Login page for the admin]
     */
    public function admin_login() {
        // If its login then redirect to the dashboard
        if($this->Auth->loggedIn()) {
            $this->redirect(array('action' => 'dashboard', 'admin' => true));
        }

        if($this->request->is('post')) {
            // If correct logged then redirect to dashboard
            if($this->Auth->login()) {
                $this->redirect(array('action' => 'dashboard', 'admin' => true));
            } else {
                $this->Session->setFlash('Usuario o contraseña inválido, intente de nuevo', 'flash_error');
            }
        }
    }

    /**
     * [admin_view description]
     * @param  [int] $id [Id of the user]
     */
    public function admin_view($id = null) {
        // Verify if the user exists on the database
        $this->User->id = $id;
        if(!$this->User->exists()) {
            throw new NotFoundException('Usuario inválido');
        }

        $isAjax = $this->RequestHandler->isAjax();
        $user = $this->User->read(null, $id);
        $this->set(compact('user', 'isAjax'));
    }

    /**
     * [admin_edit Edit a specific user]
     * @param  [int] $id [Id of the user]
     */
    public function admin_edit($id = null) {
        $this->User->id = $id;
        if(!$this->User->exists()) {
            throw new NotFoundException('Usuario inválido', 'flash_error');
        }
        
        // Only allow POST or PUT to change the data
        if($this->request->is('post') || $this->request->is('put')) {
            if($this->User->save($this->request->data)) {
                $this->Session->setFlash('El usuario ha sido salvado', 'flash_good');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('El usuario no pudo ser salvado.', 'flash_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }

        $isAjax = $this->RequestHandler->isAjax();
        $this->set(compact('isAjax'));
    }

    /**
     * [admin_add add a new user]
     */
    public function admin_add() {
        if($this->request->is('post')) {
            $this->User->create();
            if($this->User->save($this->request->data)) {
                $this->Session->setFlash('El usuario ha sido salvado', 'flash_good');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('El usuario no pudo ser salvado.', 'flash_error');
            }
        }

        $isAjax = $this->RequestHandler->isAjax();
        $this->set(compact('isAjax'));
    }

    /**
     * [admin_delete Delete logically a specific user]
     * @param  [int] $id [Id of the user]
     */
    public function admin_delete($id = null) {
        if(!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        // Verify if the use exists
        $this->User->id = $id;
        if(!$this->User->exists()) {
            throw new NotFoundException('Usuario inválido');
        }

        // Delete only logically
        //if($this->User->delete()) {
        if($this->User->saveField('deleted', 1)) {
            $this->Session->setFlash('Usuario borrado', 'flash_good');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('El usuario no fue borrado', 'flash_error');
        $this->redirect(array('action' => 'index'));
    }
    
    /**
     * [admin_logout Logout a user and redirect]
     */
    public function admin_logout() {
    	$this->redirect($this->Auth->logout());
    }
}
