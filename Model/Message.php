<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Inscriptions $Inscriptions
 */
class Message extends AppModel {
	public $useTable = false;
	
	/**
	 * Validation rules
	 *
	 * @var array
	 */
 	public $validate = array(
		'title' => array(
			'rule' => 'notEmpty'
		),
	);
}
