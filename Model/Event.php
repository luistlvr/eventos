<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Inscriptions $Inscriptions
 */
class Event extends AppModel {
	public $actsAs = array(
			'Containable',
	);
	
	var $virtualFields = array(
	    'datetime' => 'CONCAT(date, " ", start_time)'
	);

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'title';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
 	public $validate = array(
		'title' => array(
			'rule' => 'notEmpty'
		),
		'date' => array(
			'rule' => 'notEmpty'
		),
		'start_time' => array(
			'rule' => 'notEmpty',
		),
		'end_time' => array(
			'rule' => 'notEmpty',
		),
		'place' => array(
			'rule' => 'notEmpty'
		),
		'credits' => array(
			'rule' => 'numeric',
			'allowEmpty' => false
		),
		'capacity' => array(
			'numeric' => array(
                'rule'     => 'numeric',
                'message'  => 'Solo se aceptan números'
            ),
			'rule' => 'notEmpty'
		),
		'state' => array(
			'rule' => 'notEmpty'
		),
		'punishment' => array(
			'rule' => 'notEmpty'
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Inscription' => array(
			'className' => 'Inscription',
			'foreignKey' => 'event_id',
			'dependent' => true,
		), 
		'Place' => array(
			'className' => 'Place',
		), 
	);
	
	public $hasAndBelongsToMany = array(
		'Student' => array(
			'className' => 'Student',
			'joinTable' => 'inscriptions',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'student_id',
		)
	);
}
