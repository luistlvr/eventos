<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Inscriptions $Inscriptions
 */
class Place extends AppModel {
	public $actsAs = array(
			'Containable',
	);

	/**
	 * Validation rules
	 *
	 * @var array
	 */
 	public $validate = array(
		'name' => array(
			'rule' => 'notEmpty'
		),
		'capacity' => array(
			'rule' => 'numeric',
			'allowEmpty' => false
		),
	);
}
