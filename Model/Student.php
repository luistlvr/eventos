<?php
App::uses('AppModel', 'Model');
/**
 * Student Model
 *
 * @property Student $Student
 */
class Student extends AppModel {
	public $actsAs = array('Containable');
	
	// Format for the virtual field "fullname" which is a concatenation of some attributes
	var $virtualFields = array(
	    'fullname' => 'CONCAT(first_surname, " ", second_surname, ", ", names)'
	);
	
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'code' => array(
			'rule' => '/^[0-9]+(-[0-9]+)*$/i',
			'message' => 'Ingrese un código válido.'
		),
		'names' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
		'first_surname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
		'second_surname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
		'semester' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'El semestre debe ser un número.',
				'allowEmpty' => false,
			),
		),
		'mail' => array(
			'rule' => 'notEmpty',
		),
		'email' => array(
			'rule' => 'notEmpty',
		),
		'state' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'allowEmpty' => false,
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Inscription' => array(
			'className' => 'Inscription',
			'foreignKey' => 'student_id',
			'dependent' => true,
		)
	);
	
	public $hasAndBelongsToMany = array(
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'inscriptions',
			'foreignKey' => 'student_id',
			'associationForeignKey' => 'event_id',
		)
	);

	/**
	 * [beforeSave Formats some attributes before save it to the database]
	 * @param  array  $options [options]
	 * @return [ture]          
	 */
	public function beforeSave($options = array()) {
		// Formats the names to the correct format (e.g. jose eduardo => Jose Eduardo)
		if(isset($this->data[$this->alias]['names'])) {
			$this->data[$this->alias]['names'] = ucwords(strtolower($this->data[$this->alias]['names']));
		}

		// Formats the first surname to the correct format (e.g. LINUS => Linus)
		if(isset($this->data[$this->alias]['first_surname'])) {
			$this->data[$this->alias]['first_surname'] = ucwords(strtolower($this->data[$this->alias]['first_surname']));
		}

		// Formats the second surname to the correct format 
		if(isset($this->data[$this->alias]['second_surname'])) {
			$this->data[$this->alias]['second_surname'] = ucwords(strtolower($this->data[$this->alias]['second_surname']));
		}

		return true;
	}
}
