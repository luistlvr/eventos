<?php
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
	public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'El nombre de usuario es requerido'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Una contraseña es requerida'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin')),
                'message' => 'Por favor, ingrese un rol válido',
                'allowEmpty' => false
            )
        ),
    );
	
    /**
     * [beforeSave user a hash algorithm to encrypt the password]
     * @param  array  $options [some options]
     */
	public function beforeSave($options = array()) {
		if(isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}
}