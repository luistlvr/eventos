<?php
App::uses('AppModel', 'Model');
/**
 * Inscriptions Model
 *
 * @property Event $Event
 * @property Student $Student
 */
class Inscription extends AppModel {
	public $actsAs = array('Containable');
	
	/**
	 * Use table
	 *
	 * @var mixed False or table name
	 */
	public $useTable = 'inscriptions';

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'id';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'event_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'notempty' => array(
				'rule' => array('notempty'),
			),
			'unique' => array (
				'rule' => array('checkUnique', array('event_id', 'student_id')),
				'message' => 'Usted ya confirmó su inscripción al evento.',
			)
		),
		'student_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
		),
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
		)
	);
}
